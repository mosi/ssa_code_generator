import datetime
import time

import pandas as pd
import json
import numpy as np

from subprocess import check_output, CalledProcessError
import matplotlib.pyplot as plt

from matplotlib.patches import Patch
from matplotlib.lines import Line2D
import matplotlib as mpl

def get_data(file,lto : bool, single_file: bool, bionetgen : bool, dyndep: bool, sorting : bool):
    if (bionetgen and lto) or (bionetgen and single_file):
        return {}
    lto_string = " "
    if lto:
        lto_string=" --lto "

    single_file_string = " "
    if single_file:
        single_file_string=" --singlefile "

    if dyndep==1:
        single_file_string+=" --dynamic_dep "
    elif dyndep==2:
        single_file_string+=" --dynamic_dep --dynamic_rate "
    elif dyndep == 3:
        single_file_string += " --dynamic_dep --dynamic_rate --dynamic_exec"

    sort_command = " --sort "

    target="gcc"
    if bionetgen:
        target ="bionetgen"
        sort_command = " "
    if not sorting:
        sort_command = " "

    command = "timeout 60m python3 runSimulation.py --until=100   --target="+target+ sort_command +file+" --machineoutput "+lto_string+single_file_string
    print(command)
    try:
        out = check_output(command,shell=True)
        t = out
    except CalledProcessError as e:
        t = e.returncode
        return {}


    additional_times = []
    for _ in range(10):
        start = time.time()
        if not bionetgen:
            check_output("./prog_c.bin")
        else:
            check_output(
                "python3 runSimulation.py --until=100   --target=bionetgen " + file + " --machineoutput " ,  shell=True)
        end = time.time()
        additional_times.append(end - start)


    res = out.decode("utf8").split("\n")[-2]
    res = eval(res)
    res["additional_times"] = additional_times
    res["success"] = True
    return res

def generate_data():

    files = ["models/multistate.net","models/multisite2.net","models/egfr_net.net","models/BCR.net", "models/fceri_gamma2.net"]
    # (LTO, Single File

    result = []

    for _ in range(10):
        for f in files:
            for dynamic in [0,1,2,3]:
                for lto in [True,False]:
                    for bngl in [True,False]:
                        for single_file in [True,False]:
                            if not (single_file and lto) and not( (bngl and lto) or (bngl and single_file) or (bngl and dynamic)) :
                                d = get_data(file=f,lto=lto,single_file=single_file,bionetgen=bngl,dyndep=dynamic,sorting=False)
                                d["lto"] = lto
                                d["single-file"] = single_file
                                d["file"] = f
                                d["dyn dep"] = dynamic
                                d["bngl"] = bngl
                                d["sorting"] = False
                                result.append(d)
                                df = pd.DataFrame.from_dict(result)
                                df.to_csv('fin_times.csv')
                                if "success" in d.keys() and not bngl:
                                    d2 = get_data(file=f, lto=lto, single_file=single_file, bionetgen=bngl,dyndep=dynamic,sorting=True)
                                    d2["lto"] = lto
                                    d2["dyn dep"] = dynamic
                                    d2["t_compile"] += d["t_compile"]+d["t_total_run"]
                                    d2["single-file"] = single_file
                                    d2["file"] = f
                                    d2["bngl"] = bngl
                                    d2["sorting"] = True
                                    result.append(d2)
                                    df = pd.DataFrame.from_dict(result)
                                    df.to_csv('fin_times.csv')



    print(result)

    df = pd.DataFrame.from_dict(result)


def time_to_str(x):

    if x > 0:
        res = str(datetime.timedelta(seconds=float('%.2g' % x)))
        if "." in res:
            while res[-1] =="0":
                res = res[:-1]
        return res
    elif x == 0:
        return " "
    else:
        return "X"

def rot_marker(m):
    t = mpl.markers.MarkerStyle(marker=m)
    t._transform = t.get_transform().rotate_deg(310)
    return t

def plot_data(df):
    #df_1 = df[ df["file"] == "multisite2"]

    files = list(sorted(set(df["file"])))
    numx = 2
    numy = 2
    if len(files) > 4:
        numy = 3
    #print(df_1["tool"][df_1["tool"] == "BioNetGen"])
    algorithm = list(sorted( set(df["algorithm"])))
    compile = list(sorted(set(df["compile"])))
    print(compile)

    #print(algorithm)
    tools = list(sorted(set(df["tool"])))

    plt.figure(figsize=(10, 10))

    markers = {'BioNetGen':'x', 'Code Generation':r'$\mathtt{S}$', 'Dynamic':r'$\mathtt{G}$', 'Partial Code Gen. I':r'$\mathtt{2}$', 'Partial Code Gen. II':r'$\mathtt{1}$'}
    circle123 = "\N{CIRCLED DIGIT ONE}\N{CIRCLED DIGIT TWO}\N{CIRCLED DIGIT THREE}"
    #markers = [t,r'$\mathrm{G}$',r'$\mathbb{G}$','>','x']
    markers_rot = {k: rot_marker(v) for k, v in markers.items()}
    colors = {'-': 'C3', 'LTO': 'C1', 'single-file': 'C2'}

    print(files)
    files = ['multistate','multisite2', 'egfr_net', 'BCR','fceri_gamma2',  ]
    files_names = ['multistate','multisite', 'EGFR', 'BCR','Fc$\epsilon$RI',  ]
    num_reac = [9,288,3749,24388,58276]

    for (plot_num, file) in enumerate(files):
        plt.subplot(numy,numx,plot_num+1)

        plt.title(files_names[plot_num] + " ("+str(num_reac[plot_num])+" reactions)")

        cmap = plt.get_cmap('summer')
        #colors = ['C1',cmap(0),cmap(0.33),cmap(0.66),cmap(1.0)]
        df_1 = df[ df["file"] == file]
        for (nt,t) in enumerate(tools):
            for (n,c) in enumerate(compile):
                for (n1,s) in enumerate(algorithm):
                    my_df = df_1[(df_1["algorithm"] == s) & (df_1["compile"] == c)]
                    my_df = my_df[(my_df["tool"] == t)]
                    #print(t,s,c)
                    #print(my_df)
                    mark = markers[t]
                    if t == "BioNetGen":
                        mark = "o"
                        continue
                    color = colors[c]
                    facecolor = color
                    if 'optimized' in s:
                        #facecolor = 'white'
                        mark = markers_rot[t]

                    x = my_df["t_compile"]
                    y = my_df["execution"]
                    if len(my_df) == 1:
                        plt.scatter(x,y, marker=mark,facecolor=facecolor,color=color)
                    elif len(my_df) > 1:
                            plt.errorbar(x.mean(), y.mean(),xerr= x.std(),yerr=y.std(), ms=8,marker=mark,mfc=facecolor,mec=color,c=color,ecolor='0.8')
                            #plt.scatter(x.mean(),y.mean(), marker=mark,fc=facecolor,ec=color,c=color,label='test')
                    plt.xlabel("compile time [s]")
                    plt.ylabel("time per step [μs]")
        #plt.legend()
                    #print(df["compile"])
                    #my_df = my_df[my_df["tool"] == t]
    plt.subplot(numy, numx, numx*numy)
    plt.axis('off')

    color_handles = [
        Line2D([0], [0], marker='s', markeredgecolor='C3', markerfacecolor='C3', color='w',
               label='Local'),
                    Line2D([0], [0], marker='s', markeredgecolor='C1', markerfacecolor='C1', color='w',
                           label='Link time' ),
                    Line2D([0], [0], marker='s', markeredgecolor='C2', markerfacecolor='C2', color='w',
                           label='Whole program' ),]
    legend_colors = plt.legend(handles=color_handles,loc='upper left',title='Compile Optimization')

    #  {'BioNetGen':'x', 'Code Generation':r'$\mathtt{S}$', 'Dynamic':r'$\mathtt{G}$', 'Partial Code Gen. I':r'$\mathtt{2}$', 'Partial Code Gen. II':r'$\mathtt{1}$'}
    legend_elems = [Line2D([0], [0], marker=markers['Dynamic'],markeredgecolor='black', markerfacecolor='black',color='w', label='Generic', ),
                    Line2D([0], [0], marker=markers['Partial Code Gen. II'], markeredgecolor='black',
                           markerfacecolor='black', color='w', label='Partial level 1', ),
                    Line2D([0], [0], marker=markers['Partial Code Gen. I'],markeredgecolor='black', markerfacecolor='black',color='w', label='Partial level 2', ),
                     Line2D([0], [0], marker=markers['Code Generation'],markeredgecolor='black', markerfacecolor='black',color='w', label='Specialized', ),
                    Line2D([0], [0], marker=markers_rot['Dynamic'], markeredgecolor='black', markerfacecolor='black',
                           color='w', label='Generic (ODM)', ),
                    Line2D([0], [0], marker=rot_marker(markers_rot['Partial Code Gen. II']), markeredgecolor='black',
                           markerfacecolor='black', color='w', label='Partial level 1 (ODM)', ),
                    Line2D([0], [0], marker=markers_rot['Partial Code Gen. I'], markeredgecolor='black',
                           markerfacecolor='black', color='w', label='Partial level 2 (ODM)', ),
                    Line2D([0], [0], marker=markers_rot['Code Generation'], markeredgecolor='black',
                           markerfacecolor='black', color='w', label='Specialized (ODM)', ),
                    ]
    plt.gca().add_artist(legend_colors)
    plt.legend(handles=legend_elems, title='Simulator')
    plt.tight_layout()
    plt.savefig('compvrun.pdf')
    plt.show()

def print_data():

    #df = pd.read_csv('compile_times_wo_sorting.csv')
    #df["sorting"] = False

    #df2 = pd.read_csv('compile_times_intermed.csv')


    #df = df.append(df2,ignore_index=True,verify_integrity=True)
    #df = df2

    df = pd.read_csv('fin_times.csv')


    #print(df.columns)

    df = df.drop(columns=['Unnamed: 0', 't_parsing', 't_sort','t_reported'])

    df["t_compile"].where( df["bngl"] == False,0,inplace=True)

    df["tool"] = ""
    df["tool"].where(df["bngl"]==False,"BioNetGen",inplace=True)
    df["tool"].where(df["bngl"] == True, "Code Generation", inplace=True)
    df["tool"].where(df["dyn dep"] != 1, "Partial Code Gen. I", inplace=True)
    df["tool"].where(df["dyn dep"] != 2, "Partial Code Gen. II", inplace=True)
    df["tool"].where(df["dyn dep"] != 3, "Dynamic", inplace=True)




    df["compile"] = "-"
    df["compile"].where(df["lto"] == False, "LTO", inplace=True)
    df["compile"].where(df["single-file"] ==False , "single-file", inplace=True)

    df["algorithm"] = "-"
    df["algorithm"].where(df["sorting"] == True, "DM", inplace=True)
    df["algorithm"].where(df["sorting"] == False, "optimized DM", inplace=True)
    df["algorithm"].where(df["bngl"] == False, "sorting DM", inplace=True)

    df["file"] = df["file"].apply(lambda x: x.replace("models/","").replace(".net",""))


    df["additional_times"] = df["additional_times"].apply(lambda x: np.array(json.loads(x)) if "[" in str(x) else np.array([]))
    df["execution"] = df["additional_times"].apply(lambda x: x.mean())


    df.loc[df["tool"]=="Dynamic","t_compile"] = 0
    #TODO fIx line below
    #print(df.loc[(df["tool"] == "Dynamic") & (df["sorting"] == True)])
    #print( df.loc[(df["tool"] == "Dynamic") & (df["sorting"] == False)])
    df.loc[(df["tool"] == "Dynamic") & (df["sorting"] == True), "t_compile"] = np.array(df.loc[(df["tool"] == "Dynamic") & (df["sorting"] == False) & df["execution"] > 0, "execution"])
    #print(np.array(df.loc[(df["tool"] == "Dynamic") & (df["sorting"] == False), "execution"]))

    df["execution"] = df["execution"]/ df["num_steps"] * 1e6
    plot_data(df)


    df.to_csv("test_output.csv")


    df = df.groupby(["file","tool","compile","algorithm"]).mean()



    #print(df.columns)
    #print(df[["t_compile","t_total_run"]])
    df2 = df[["t_compile","execution","num_steps"]]

    df2["t_compile"] = df2["t_compile"].apply(time_to_str)
    df2["execution"] = df2["execution"].apply(time_to_str)
    pd.set_option("display.max_rows", None, "display.max_columns", None)
    print(df2)
    print(df2.to_latex(float_format="{:0.3g}".format))

    #print()

generate_data()
print_data()
