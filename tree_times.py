import datetime
import time

import pandas as pd
import json
import numpy as np
from scipy import stats

from subprocess import check_output, CalledProcessError

import matplotlib.pyplot as plt


def get_times(modelname,sort : bool,extra_opt : bool,tree: bool,const_replace: bool):
    times = []

    df = pd.DataFrame()

    options = ""
    if sort:
        options += " --sort "
    if extra_opt:
        options += " --lto --singlefile "

    if not const_replace:
        options += " --dontreplaceconstants "

    if tree:
        options += " --tree "

    for _1 in range(1):
        print(options)
        check_output("python3 runSimulation.py --until=100 --target=gcc "+options + " "+modelname, shell=True)


        for _ in range(10):
            start = time.time()
            check_output("./prog_c.bin")
            end = time.time()
            df = df.append({'model':modelname,'times':end -start,'sort' : sort,'extra_opt':extra_opt, 'const_replace' : const_replace, 'tree':tree},ignore_index=True)
            times.append(end - start)
    return df

def get_all_for_model(modelname,xtra):
    ntree = np.array(get_times(modelname,False,xtra,False))
    tree = np.array(get_times(modelname,False,xtra,True))



    res1 = modelname + "tree: "+str(tree.mean())+" +- " + str(stats.sem(tree))
    res2 = modelname + "not tree: " + str(ntree.mean()) + " +- " + str(stats.sem(ntree))

    print(res1)
    print(res2)
    ntree = np.array()
    tree = np.array(get_times(modelname, True, xtra, True))

    res3 = modelname + "sorted tree: " + str(tree.mean()) + " +- " + str(stats.sem(tree))
    res4 = modelname + "sorted not tree: " + str(ntree.mean()) + " +- " + str(stats.sem(ntree))
    print(res3)
    print(res4)


    return res1+ "\n" + res2 + "\n"+ res3+ "\n"+res4+ "\n"

def gen_data():
    df = pd.DataFrame()

    for _ in range(10):
        df = df.append(get_times('models/multisite2.net', extra_opt=True,tree= False,const_replace=True,sort=False),ignore_index=True)
        df = df.append(get_times('models/multisite2.net', extra_opt=True,tree= True,const_replace=True,sort=False),ignore_index=True)
        df = df.append(get_times('models/multisite2.net', extra_opt=True,tree= False,const_replace=False,sort=False),ignore_index=True)

        df.to_csv('opt_testing.csv')
        print(df)

        df2 = df.groupby(["model","sort","extra_opt","const_replace","tree"]).mean()
        print(df2)
    print(df2.to_latex(float_format="{:0.3g}".format))

    check_output("python3 runSimulation.py --until=100 --target=gcc models/egfr_net.net", shell=True)
    for _ in range(10):
        df = df.append(get_times('models/egfr_net.net', extra_opt=False,tree= False,const_replace=True,sort=True),ignore_index=True)
        df = df.append(get_times('models/egfr_net.net', extra_opt=False,tree= True,const_replace=True,sort=True),ignore_index=True)
        df = df.append(get_times('models/egfr_net.net', extra_opt=False,tree= False,const_replace=False,sort=True),ignore_index=True)

    df.to_csv('opt_testing.csv')
    print(df)

def print_data():
    df = pd.read_csv('opt_testing.csv')

    df2 = df.groupby(["model","sort","extra_opt","const_replace","tree"]).mean()
    print(df2)

    df2 = df.groupby(["model","sort","extra_opt","const_replace","tree"])['times'].apply(list)
    print(df2)

    for a,b in df2.iteritems():
        print(a[0])
        if 'egfr' in str(a[0]):
            print(b)
            plt.hist(b,label=str(a),bins=20,histtype='step',density=True)
        #plt.hist(a,b)
    plt.legend()
    plt.show()



#res += get_all_for_model('models/multisite2.net',True)
#res = get_all_for_model('models/egfr_net.net',False)
#res = get_all_for_model('models/BCR.net',False)

print_data()