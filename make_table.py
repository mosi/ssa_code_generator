from glob import glob

import pandas as pd

from python_src.benchmarks import model2toml
from python_src.benchmarks import generate_parsed, optimize_model_order, fix_numbers, generate_rust_code, \
    generate_cpp_code, model2toml
from python_src.bngl_net_parser import shuffle_model, find_dependencies
import subprocess
import json


def get_loc(files):
    sum = 0
    for f in files:
        out = subprocess.check_output("tokei -o json " + f, shell=True)
        print(out)
        vals = json.loads(out.decode("utf-8"))#["Cpp"]
        print(vals)
        assert (len(vals) == 1)
        sum += vals['Cpp']['code']
        #for rs in vals:
        #    # print(vals[rs])
        #    sum += vals[rs]['code']
        # print(vals)
    return sum

def give_table_of_models():
    df = pd.DataFrame()

    for model_file in glob('models/*.net'):
        m = model2toml(model_file,replace_constants=True)
        find_dependencies(m)
        m["num_steps"] = 1000  # never used
        m["until"] = 20.0  # never used
        m["linear"] = True
        m["log"] = 0
        m["dynamic_dependencies"] = False
        m["dynamic_rates"] = False
        m["dynamic_execution"] = False

        try:
            generate_cpp_code(m, "hhg++", lto=False, use_single_file=True)
        except:
            print("Failed as expected")
        ave_num_affected = 0
        loc_cpp = get_loc(glob("cpp/all_merged.cpp"))
        #loc_rust = get_loc(glob("cpp/*.rs"))
        #loc = "{:d}".format(int(round_sig((loc_cpp+loc_rust)/2,3)))

        #print(m)
        for r in m["rules"]:
            #print(r)
            print(len(r['affected_rules']))
            ave_num_affected += len(r['affected_rules'])
        ave_num_affected /= len(m["rules"])
        result = {'model': model_file.replace('models/', '').replace('.net', ''), 'reactions': len(m["rules"]),
                  'species': len(m["species"]), "LoC":loc_cpp , "degree": ave_num_affected}
        df = df.append(result, ignore_index=True)
        print(df)



    df = df.set_index('model')
    df = df.sort_values('LoC')
    df = df.astype('int')
    df = df.sort_values(df.index[1], axis=1)
    df['LoC'] = df['LoC'].apply(lambda x: '~'+str(x))
    df.to_csv('table_of_models.csv')
    print(df.to_latex(index=True, escape=True))

give_table_of_models()