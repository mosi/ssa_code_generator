import pandas as pd
import os
import random

from plotRuntimeData import plot_runtimes
from runSimulation import core_execution_function

target_time = 100.0
iosteps = 1000
df = pd.DataFrame()

if(os.path.isfile("totalRuntimes.csv")):
    df = pd.read_csv("totalRuntimes.csv")


while True:
    for modelname in ['models/multisite2.net', 'models/multistate.net', 'models/egfr_net.net']:
            reps = 5
            if("egfr" in modelname):
                reps = 1
            for _ in range(reps):
                order = list(["generic", "bionetgen", "rust", "gcc" ,"clang"])
                random.shuffle(order)
                for simulator in order:
                    for do_sort in [False, True]:
                        if do_sort and simulator == "bionetgen":
                            continue
                        res = core_execution_function(modelfile=modelname, target=simulator, steps=iosteps, until=target_time, optimize=do_sort)
                        res["Simulator"] = simulator
                        res["do_sort"] = do_sort
                        res["until"] = target_time
                        res["iosteps"] = iosteps
                        res["model"] = modelname
                        df = df.append(res, ignore_index=True)
                        df.to_csv("totalRuntimes.csv", index= False)
                        plot_runtimes("totalRuntimes.csv")
