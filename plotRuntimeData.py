#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def plot_runtimes(filename):
    df = pd.read_csv(filename)


    df['model'] = df['model'].apply(lambda x: x.replace("models/", "").replace(".net", "").replace('_net', ''))
    df["model"] = df["model"].apply(
        lambda x: x.replace('networks/', '').replace('models/', '').replace('.reaction_model', '').replace('_', '').replace(
            'gen', '').replace('site2', 'site'))

    df = df[df["model"].apply(lambda x: not 'beta' in x)]
    # df = df[df["model"].apply(lambda x: 'beta' in x)]

    df["Simulator"] = df["Simulator"].apply(lambda x: x.replace('parsed', 'generic'))#.replace('specialized', 'special.').replace('compiled', 'special.').replace('rust','special.'))
    df["Simulator"] = df["Simulator"].apply(lambda x: 'specialized ' + x if not ('bionet' in x or 'generic' in x) else x)

    df['do_sort_str'] = df['do_sort'].astype('bool').apply(lambda x: 'ODM' if x else '')
    printer_mean = df.groupby(['model', 'Simulator', 'do_sort_str']).mean()

    assert(not printer_mean.isnull().values.any())

    if(printer_mean.isnull().values.any()):
        print("Not enough values to determien Error everywhere!!! replaced with error = 0")
        printer_mean.fillna(0)

    printer_sem = df.groupby(['model', 'Simulator', 'do_sort_str']).sem()  # [["t_compile","t_total_run","t_sort"]]

    # printer_mean['t_compile'].as_type('str') + printer_std['t_compile'].as_type('str')
    # print( printer_mean['t_compile'].map(str) + ' +- '+printer_std['t_compile'].map(str))

    printer = pd.DataFrame()


    def first_digit_is_1(val):
        v2 = val
        while (v2 < 10):
            v2 *= 100
        return str(v2)[0] == '1'


    def print_number_w_error(val, err):

        if(np.isnan(err)):
            err = val
            print("WARING: Too few runs to determine error. Therefore error is value")

        if val < .001:
            return ""

        num_err = 1
        if first_digit_is_1(err):
            num_err = 2

        while float(np.format_float_positional(err, precision=num_err, unique=False, fractional=False, trim='k')) < err:
            err *= 1.001

        err = np.format_float_positional(err, precision=num_err, unique=False, fractional=False, trim='k')

        num_err = 1
        if first_digit_is_1(float(err)):
            num_err = 2

        err2 = np.format_float_positional(float(err), precision=num_err, unique=False, fractional=False, trim='k').rstrip(
            '.')
        err = float(err2)

        num_signif = str(int(np.floor(np.log10(val)) - np.floor(np.log10(err))) + num_err)

        err2 = ("{:#." + str(num_err) + "G}").format(err).rstrip(".")

        return str("{:#." + num_signif + "G}").format(val).rstrip(".") + " $ \pm $ " + err2


    def get_merged(A, B, name):
        d = pd.DataFrame()
        d['one'] = A[name]
        d['two'] = B[name]

        return d.apply(lambda x: print_number_w_error(x['one'], x['two']), axis=1)


    printer = pd.DataFrame()
    printer['compile'] = get_merged(printer_mean, printer_sem, 't_compile')

    printer['runtime'] = get_merged(printer_mean, printer_sem, 't_total_run')
    printer['sorting'] = get_merged(printer_mean, printer_sem, 't_sort')

    table = printer.to_latex(index=True, escape=False)
    with open("runtimeData_table.txt",'w') as f:
        f.write(table)
    print(table)

    models = set(df['model'])
    num_of_models = len(models)

    mw = .8
    repeats = [200, 50, 20]
    alphas = [1, .8, .4]


    res_frame = pd.DataFrame()



    def sort_func(x):
        if "bionet" in x:
            return -100
        res = 0
        if "ODM" in x:
            res += .5
        if "generic" in x:
            res += 1
        if "gcc" in x:
            res += 2

        if "clang" in x:
            res += 3
        if "rust" in x:
            res += 4
        return res


    def sort_func2(x):
        res = 0

        if "pred" in x:
            return -3
        if "multistate" in x:
            return -2
        if "multisite2" in x:
            return -1

        return res


    for mod_num, model in enumerate(sorted(models, key=sort_func2)):
        print("\nModel = " + model)
        df2 = df[df['model'] == model].copy()
        df2["full sim"] = " sort=" + df2["do_sort"].astype(str) + df["Simulator"]
        df2["full sim"] = df2["full sim"].apply(lambda x: x.replace('sort=1.0', 'ODM '))
        df2["full sim"] = df2["full sim"].apply(lambda x: x.replace('sort=0.0', ''))
        sims = list(sorted(set(df2["full sim"]), key=sort_func))
        # sims = set(filter(lambda x: not "compiled" in x,sims))

        runtime = []
        t_sort = []
        t_parsing = []
        t_compile = []
        t_reported = []

        for s in sims:
            df3 = df2[df2['full sim'] == s]

            # print(s)
            runtime.append(df3['t_total_run'].mean())
            t_sort.append(df3['t_sort'].mean())
            t_parsing.append(df3['t_parsing'].mean())
            t_compile.append(df3['t_compile'].mean())
            t_reported.append(df3['t_reported'].mean())
            print(s + "\t -> " + str(df3['num_steps'].mean()) + " +- " + str(df3['num_steps'].std()))


        t_sort = np.array(t_sort)
        t_parsing = np.array(t_parsing)
        t_compile = np.array(t_compile)
        t_reported = np.array(t_reported)

        # plt.subplot(num_of_models+1,1,mod_num+1)
        plt.subplot(2, 2, mod_num + 1)
        plt.xlabel('time [s]')
        # plt.ylim(0,7)
        plt.title(model)

        ind = np.arange(len(sims))

        plt.yticks(ind, sims)  # ,rotation=90)
        plt.barh(ind, runtime, label='Runtime')  # ,color='C0')
        plt.barh(ind + .3, t_reported, height=.2, label='Reported')  # ,color='C9')

        sep = mw / len(repeats)
        for (n, r) in enumerate(repeats):
            plt.barh(ind + sep * n - sep, (t_sort + t_compile) / r, height=mw / len(repeats), color='C4', alpha=alphas[n],
                     left=runtime, label='Comp. (' + str(r) + ')')

        (a, b) = plt.gca().get_ylim()
        # plt.ylim(0,b*1.5)

        # plt.legend()

    # plt.subplot(num_of_models+1,1,num_of_models+1)
    plt.subplot(2, 2, num_of_models + 1)
    plt.bar([1], [1], label='Runtime')
    plt.bar([1], [1], label='Reported CPU time', width=.4)
    plt.ylim([10, 11])

    for (n, r) in enumerate(repeats):
        plt.bar([1], [1], label='Overhead (' + str(r) + ' reps)', color='C4', alpha=alphas[n], )
    # plt.gcf().patch.set_visible(False)
    plt.gca().axis('off')

    plt.tight_layout()
    plt.gcf().set_size_inches(10, 7, forward=True)

    plt.tight_layout()
    plt.legend(loc='upper left')  # ,bbox_to_anchor=(-0.6, 1.4),facecolor='white', framealpha=1)

    plt.savefig('Runtime_plot.pdf')
    #plt.show()

    plt.gcf().clear()

    iterations = np.linspace(0, 200, num=1000)
    for mod_num, model in enumerate(sorted(models, key=sort_func2)):
        plt.gcf().set_size_inches(5, 5, forward=True)
        print("\nModel = " + model)
        plt.title(model)
        plt.xlabel('Simulation Iterations')
        plt.ylabel('Total sequential runtime [s]')
        df2 = pd.DataFrame()
        df2 = df[df['model'] == model].copy()
        df2["full sim"] = " sort=" + df2["do_sort"].astype(str) + df["Simulator"]
        df2["full sim"] = df2["full sim"].apply(lambda x: x.replace('sort=1.0', 'ODM '))
        df2["full sim"] = df2["full sim"].apply(lambda x: x.replace('sort=0.0', ''))
        sims = list(sorted(set(df2["full sim"]), key=sort_func))

        for s in sims:
            df3 = df2[df2['full sim'] == s]

            plt.plot(iterations,
                     iterations * df3['t_total_run'].mean() + df3['t_parsing'].mean() + df3['t_compile'].mean() + df3[
                         't_sort'].mean(), label=s)
        plt.legend()
        plt.tight_layout()
        plt.savefig('linear_time_' + model + '.pdf')
        #plt.show()

if __name__ == "__main__":
    # use data as generated by script
    name = 'totalRuntimes.csv'

    # use provided sample data
    #name = 'sampleTotalRuntimes.csv'

    plot_runtimes(name)