from runSimulation import core_execution_function


modelfile = "models/multistate.net"
steps = 100
until = 10

for t in ["rust","gcc","clang","generic"]:
    core_execution_function(modelfile, True, steps, until=until,target=t)
core_execution_function(modelfile,False,steps=steps,until=until,target="bionetgen")

print("success!")