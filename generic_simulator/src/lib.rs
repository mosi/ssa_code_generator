use rand::prelude::*;
use rand::rngs::SmallRng;
use std::fs::File;
//use std::fs::*;
use std::io::*;
use std::path::Path;

//const MEASURE_REACTION_NUMS: bool = false;

#[derive(Clone, Debug)]
struct Rule {
    rate: f64,
    rate_pops: Vec<usize>,
    rem_spec: Vec<usize>,
    add_spec: Vec<usize>,
    min_amounts: Vec<(usize, usize)>,
    pub affected_reactions: Vec<usize>,
    idx: usize,
}
impl Rule {
    //TODO Update for special A + A -> tpye reaction
    //pub fn update_rate(&self, _m: &mut ModelStruct) {}
}

#[derive(Clone, Debug)]
pub struct ModelStruct {
    pub propensity: Vec<f64>,
    pub populations: Vec<usize>,
    pub propensity_sum: f64,

    rules: Vec<Rule>,

    pub time: f64,
    //rng: ThreadRng,// Thread Rng is slower but more "Random"
    rng: SmallRng,
    pub step_counter: usize,

    count_reactions: bool,
    pub reaction_counts: Vec<usize>,
    pub observations: Vec<Vec<usize>>,
    pub observation_defs: Vec<Vec<usize>>,
    pub obs_names: Vec<String>,

    pub num_steps: usize,
    pub time_step: f64,
    pub delta_t: f64,
    pub until: f64,
}

impl ModelStruct {
    pub fn new(p: &Path, count_reactions: bool) -> ModelStruct {
        let s = std::fs::read_to_string(p).expect(&format!("File {} not found", p.display()));
        let dat: toml::value::Table = toml::from_str(&s).expect("Could not parse to toml!");

        let obs_def = dat["observations"].as_table().unwrap();
        let obs_def: Vec<Vec<usize>> = obs_def
            .values()
            .map(|v| {
                v.as_array()
                    .unwrap()
                    .iter()
                    .map(|x| x.as_integer().unwrap() as usize)
                    .collect()
            })
            .collect();

        let rules: Vec<Rule> = dat["rules"]
            .as_array()
            .unwrap()
            .iter()
            .map(|v| v.as_table().unwrap())
            .enumerate()
            .map(|(ref_num, r)| {
                assert_eq!(ref_num, r["num"].as_integer().unwrap() as usize);
                r
            })
            .map(|r| Rule {
                rate: r["rate_factor"].as_float().unwrap(),
                rate_pops: r["rate_species"]
                    .as_array()
                    .unwrap()
                    .iter()
                    .map(|idx| idx.as_integer().unwrap() as usize)
                    .collect(),
                rem_spec: r["opt_in"]
                    .as_array()
                    .unwrap()
                    .iter()
                    .map(|idx| idx.as_integer().unwrap() as usize)
                    .collect(),
                add_spec: r["opt_out"]
                    .as_array()
                    .unwrap()
                    .iter()
                    .map(|idx| idx.as_integer().unwrap() as usize)
                    .collect(),
                min_amounts: if r.contains_key("min_of") {
                    r["min_of"]
                        .as_table()
                        .expect("Minof not found")
                        .iter()
                        .map(|(mkey, val)| {
                            (
                                mkey.parse::<usize>().expect("Conversion failed"),
                                val.as_integer().expect("Not found key!!") as usize,
                            )
                        })
                        .collect()
                } else {
                    vec![]
                },
                affected_reactions: dat["affected_reaction"].as_table().unwrap()
                    [&r["num"].as_integer().unwrap().to_string()]
                    .as_array()
                    .unwrap()
                    .iter()
                    .map(|aff_v| aff_v.as_integer().unwrap() as usize)
                    .collect(),
                idx: r["num"].as_integer().unwrap() as usize,
            })
            .collect();

        let _num_rules = rules.len();

        let mut m = ModelStruct {
            propensity: vec![0.0; rules.len()],
            populations: dat["init"]
                .as_array()
                .unwrap()
                .iter()
                .map(|v| v.as_integer().unwrap() as usize)
                .collect(),
            propensity_sum: 0.0,
            rules: rules,
            time: 0.0,
            rng: SmallRng::from_entropy(),
            observations: vec![vec![]; obs_def.len()],
            until: dat["until"].as_float().expect("'until' not found"),
            num_steps: dat["num_steps"].as_integer().unwrap() as usize,
            time_step: 0.0,
            step_counter: 0,
            delta_t: 0.0,
            observation_defs: obs_def,
            obs_names: dat["observations"]
                .as_table()
                .unwrap()
                .keys()
                .cloned()
                .collect(),
            reaction_counts: {
                if count_reactions {
                    vec![0; _num_rules]
                } else {
                    vec![]
                }
            },
            count_reactions,
        };
        m.delta_t = m.until / m.num_steps as f64;
        m.init();

        m
    }

    pub fn init(&mut self) {
        for i in 0..self.rules.len() {
            self.update_reac_i(i);
        }
    }

    pub fn get_random_uniform_propensity(&mut self) -> f64 {
        self.rng.gen_range(0.0, self.propensity_sum)
    }
    pub fn get_needed_random_numbers(&mut self) -> (f64, f64) {
        if self.propensity_sum <= 0.0 {
            panic!("No reaction possible!")
        }
        let distr = rand_distr::Exp::new(self.propensity_sum).unwrap();
        let timestep = distr.sample(&mut self.rng);
        (timestep, self.get_random_uniform_propensity())
    }

    pub fn select_next(&mut self, max: f64) -> usize {
        self.propensity
            .iter()
            .scan(0.0, |sum, p| {
                *sum += p;
                Some(*sum)
            })
            .position(|p| p > max)
            .expect("No reaction was selected")
    }

    pub fn update_reac_i(&mut self, i: usize) {
        let rule = &self.rules[i];
        self.propensity_sum -= self.propensity[i];
        self.propensity[i] = rule.rate;
        for other_id in rule.rate_pops.iter() {
            self.propensity[i] *= self.populations[*other_id] as f64;
        }
        for (spec, amt) in rule.min_amounts.iter() {
            if self.populations[*spec] < *amt {
                self.propensity[i] = 0.0;
            }
        }
        self.propensity_sum += self.propensity[i];
    }

    pub fn execute(&mut self, x: usize) {
        if self.count_reactions {
            self.reaction_counts[x] += 1;
        }
        for i in self.rules[x].add_spec.iter() {
            self.populations[*i] += 1;
        }
        for i in self.rules[x].rem_spec.iter() {
            self.populations[*i] -= 1;
        }

        for i in self.rules[x].affected_reactions.iter() {
            self.propensity_sum -= self.propensity[*i];
            self.propensity[*i] = self.rules[*i].rate;
            for other_id in self.rules[*i].rate_pops.iter() {
                self.propensity[*i] *= self.populations[*other_id] as f64;
            }
            self.propensity_sum += self.propensity[*i];
        }
    }

    pub fn print_final_counts(&self, p: &Path) {
        assert!(self.count_reactions);

        let mut file = File::create(p).unwrap();
        match write!(
            file,
            "reac_counts = {}",
            toml::ser::to_string(&self.reaction_counts).unwrap()
        ) {
            Ok(_) => (),
            Err(_) => panic!("Error writing file"),
        }
    }

    pub fn step(&mut self) {
        self.step_counter += 1;
        let (timestep, max) = self.get_needed_random_numbers();
        self.time += timestep;
        let x = self.select_next(max);

    }

    pub fn print_first_line(&self) {
        println!("time S W ");
    }

    pub fn run_until(&mut self, target_time: f64) {
       // temporory test
        while self.time < target_time /*&& self.num_steps < 100000*/ {
            self.step();
        }
    }
    pub fn model_observe(&mut self) {
        let tmp: Vec<usize> = self
            .observation_defs
            .iter()
            .map(|obs| obs.iter().map(|idx| self.populations[*idx]).sum())
            .collect();
        tmp.into_iter()
            .enumerate()
            .for_each(|(n, v)| self.observations[n].push(v));
    }
}
