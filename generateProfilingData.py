import json
import os
import shutil
import subprocess
from glob import glob
from random import random

import pandas as pd

from plotProfilingData import plotProfilingData
from python_src.benchmarks import model2toml, generate_rust_code, generate_parsed, optimize_model_order
from python_src.bngl_net_parser import shuffle_model, find_dependencies
from runSimulation import core_execution_function

def get_rust_bench_comp_times(path_to_code):
    assert(os.path.isdir(path_to_code))
    times = {}
    try:
            shutil.rmtree(path_to_code+"/target/criterion")
    except:
            pass
    #//out = subprocess.check_output('cargo bench -j 1 --manifest-path='+path_to_code+'/Cargo.toml', shell=True,
    #//                             stderr=subprocess.STDOUT)
    assert(os.system('cargo bench -j 1 --manifest-path='+path_to_code+'/Cargo.toml')==0)
    list_of_benches = glob(path_to_code+"/target/criterion/*")

    for lb in list_of_benches:
        lbname = lb.split("/")[-1]
        if not "report" in lb:
            times[lbname] = {}
            if "only" in lb:
                sub_bench = [lb, ]
            else:
                sub_bench = glob(lb + "/*")
            for b in sub_bench:
                if not "report" in b:
                    bname = b.split("/")[-1]
                    # print("read in "+b+" @ "+bname)
                    # print("lb = "+lb+" b = "+b)
                    times[lbname][bname] = json.load(open(b + "/new/estimates.json"))
                    # print(b)

    result_vec = []
    # pp = pprint.PrettyPrinter(depth=6)
    # pp.pprint(times)

    for bench in times.keys():
        for sub_bench in times[bench]:
            new_res = {}
            if sub_bench != bench:
                new_res["steps"] = sub_bench
            for statistical_quantity in times[bench][sub_bench].keys():
                for par in times[bench][sub_bench][statistical_quantity].keys():
                    my_dict = times[bench][sub_bench][statistical_quantity][par]
                    if isinstance(my_dict, dict):
                        for k in my_dict.keys():
                            new_res[bench + " " + statistical_quantity + " " + par + " " + k] = my_dict[k]
                    else:
                        new_res[bench + " " + statistical_quantity + " " + par] = my_dict
            result_vec.append(new_res)

    return result_vec



def profiling_benchmarks():
    df = pd.DataFrame()
    for model_file in ['models/predPrey.net', 'models/multisite2.net', 'models/multistate.net', 'models/egfr_net.net']:
        reps = 10
        if ("egfr" in model_file):
            reps = 2
        for _ in range(reps):
            target = ["generic","rust"]
            #random.shuffle(target)
            for s in target:
                for opt in [True,False]:
                    core_execution_function(modelfile=model_file, optimize=opt, steps=1000, until=20.0, target=s, only_generate=True)
                    path_to_code = "generic_simulator"
                    if (s == "rust"):
                        path_to_code = "generated_code"
                    measuerments = get_rust_bench_comp_times(path_to_code=path_to_code)
                    for i in measuerments:
                        i["model"] = model_file.split("/")[1].replace(".net","")
                        i["simulator"] = s
                        i["guided opt"] = opt
                        i["compiled"] = s == "rust"
                    df = df.append(measuerments, ignore_index=True)
                    df.to_csv("profilingData.csv")
                    plotProfilingData("profilingData.csv")

if __name__ == "__main__":
    while(True):
        profiling_benchmarks()