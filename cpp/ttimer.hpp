#include <cassert>
#include <chrono>

#pragma once

struct ttimer {
  std::chrono::time_point<std::chrono::high_resolution_clock> startTime, end;
  bool running = false;
  std::chrono::high_resolution_clock::duration totalTimePast = std::chrono::high_resolution_clock::duration::zero();
  void start() {
    assert(!running);
    running = true;
    startTime = std::chrono::high_resolution_clock::now();
  }

  double timePassed() const {
    auto now = std::chrono::high_resolution_clock::now();
    assert(running);
    std::chrono::duration<double> totalTime = now - startTime;
    return totalTime.count();
  }

  void reset() {
    assert(!running);
    totalTimePast = std::chrono::high_resolution_clock::duration::zero();
  }

  void restart() {
    assert(running);
    running = false;
    start();
  }

  void stop() {
    end = std::chrono::high_resolution_clock::now();
    assert(running);
      std::chrono::high_resolution_clock::duration totalTime = end - startTime;
    totalTimePast += totalTime;
    running = false;
  }
  double timeInSec() {
    assert(!running);


    return std::chrono::duration<double>(totalTimePast).count();
  }
};
