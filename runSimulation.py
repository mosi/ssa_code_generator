import argparse

import jinja2
import toml
import subprocess
import os
from timeit import default_timer as timer
import json
import random
import pathlib

from prettytable import PrettyTable

from python_src.benchmarks import generate_parsed, optimize_model_order, fix_numbers, generate_rust_code, \
    generate_cpp_code, model2toml
from python_src.bngl_net_parser import shuffle_model, find_dependencies



print_output = True


def dump_constants(model):
    res_int = {}
    for i in model["int_constants"]:
        try:
            res_int[i]= model["int_constants"][i]["default"]
        except KeyError:
            res_int[i] = -1

    res_float = {}
    for i in model["float_constants"]:
        try:
            res_float[i]= model["float_constants"][i]["default"]
        except KeyError:
            res_float[i] = -1.0
    result = {"int_constants" : res_int,"float_constants" : res_float}
    with open('constants.json', 'w') as outfile:
        json.dump(result, outfile)

def get_bionetgen_path():
    bnglpath = 'BioNetGen-2.5.0/bin/run_network'

    if (not os.path.isfile(bnglpath)):
        print("# could not find bionetgen. Try autodownloaiding")
        os.system(
            'wget https://github.com/RuleWorld/bionetgen/releases/download/BioNetGen-2.5.0/BioNetGen-2.5.0-linux.tgz')
        os.system('tar xvzf  BioNetGen-2.5.0-linux.tgz')
    if (not os.path.isfile(bnglpath)):
        raise Exception(bnglpath + " does not exist")

    return bnglpath


def core_execution_function(modelfile,optimize,steps,until,target,lto:bool,tauleaping : bool,only_generate=False, use_single_file = False, use_dynamic_dependencies = False, use_dynamic_rates = False, use_dynamic_exec = False, use_tree=False,replace_constants_in_bngl=True):
    if( not os.path.isfile(modelfile)):
        raise Exception("File does not exist: "+modelfile)

    extention = pathlib.Path(modelfile).suffix

    if(use_tree and tauleaping):
        raise Exception("can not use tree and tau")

    if (target == "bionetgen" and optimize):
        raise Exception("Bionetgen is not compatible with optimization option")


    assert(steps > 1)
    assert(until > 0)
    if(not target in ["rust","gcc","generic","clang","bionetgen","python"]):
        raise Exception("Unsuported target: "+target)

    if (not extention in [".json",".net",".reaction_model"]):
        raise Exception("Unknown file extention "+extention)

    start_parse = timer()

    if(extention == ".json"):
        m = json.load(open(modelfile))
    else:
        m = model2toml(modelfile,replace_constants_in_bngl)
        json.dump(m,open('reaction_model_parsed.json','w'))


    #m = shuffle_model(m)
    unprocesd_model = m.copy()

    find_dependencies(m)
    stop_parse = timer()

    m["num_steps"] = int(steps)
    m["until"] = float(until)
    m["linear"] = not use_tree
    m["log"] = 3

    m["dynamic_dependencies"] = use_dynamic_dependencies
    m["dynamic_rates"] = use_dynamic_rates
    m["dynamic_execution"] = use_dynamic_exec

    dump_constants(m)






    def check_rust():
        try:
            subprocess.check_output('cargo -V', shell=True, stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError:
            raise Exception("Rust does not work. 'cargo -V' failed")

    start_sort = timer()
    if(optimize):
        print("# Preparing counting simulator")
        """
        check_rust()
        subprocess.check_output('cargo build --manifest-path=generic_simulator/Cargo.toml --release', shell=True, stderr=subprocess.STDOUT)

        if os.path.exists("reaction_counts.toml"):
            os.remove("reaction_counts.toml")
        #generate_parsed(m)
        m["num_steps"] = int(steps)
        m["until"] = float(until)
        toml.dump(fix_numbers(m), open("temporary_model.toml", "w"))

        print("# Running counting simulator")
        subprocess.check_output('cargo run --manifest-path=generic_simulator/Cargo.toml --release -- temporary_model.toml --count', shell=True,
                                stderr=subprocess.STDOUT)
        """
        if( os.path.exists("reaction_counts.toml")):
            m = optimize_model_order(unprocesd_model, "reaction_counts.toml")
            print("Used full run for reaction counts")
        elif (os.path.exists("reaction_counts_partial.toml")):
            m = optimize_model_order(unprocesd_model, "reaction_counts_partial.toml")
            print("Used partial run for reaction counts")

        find_dependencies(m)
        m["num_steps"] = int(steps)
        m["until"] =  float(until)
        stop_sort = timer()
    else:
        stop_sort = start_sort

    m["num_steps"] = int(steps)
    m["until"] = float(until)
    m["linear"] = not use_tree
    m["log"] = 0

    m["tau"] = tauleaping

    m["dynamic_dependencies"] = use_dynamic_dependencies
    m["dynamic_rates"] = use_dynamic_rates
    m["dynamic_execution"] = use_dynamic_exec



    print("# running with: "+target)
    m["count_rules"] = True

    start_compile = timer()

    # Create reaction dependency file
    if m["dynamic_dependencies"] :
        dd = [[]]* len(m["rules"])
        lookup = {}
        for r in m["rules"]:
            lookup[r["uid"]] = r["num"]
        for r in m["rules"]:
            l = []
            for dep in r["affected_rules"]:
                l.append(lookup[dep])
            #print(dd)
            dd[r["num"]] = l
        with open('dynamic_dependencies.json', 'w') as outfile:
            json.dump(dd, outfile)

    if m["dynamic_rates"] or m["dynamic_execution"]:
        dd = [[]]* len(m["rules"])

        for r in m["rules"]:
            spec = []
            assert(len(r["float_constants_in_rate"])==0)
            rate_expr = r["rate_expression"]
            for s in r["species_in_rate"]:
                s2 = "__species__:"+s
                while s2 in rate_expr:
                    spec.append(int(m["species"][s]["num"]))
                    rate_expr = rate_expr.replace(s2,"1",1)

            #print(dd)
            #print(rate_expr)
            while rate_expr.endswith("*1"):
                rate_expr = rate_expr[:-len("*1")]
            rate_expr = rate_expr.replace("*1*", "*")
            #print("-> ",rate_expr)

            spec_single = set(spec)
            spec_single = list(spec_single)

            amts = [-1] * len(spec_single)
            for i in range(len(amts)):
                amts[i] = spec.count(spec_single[i])
            cons = list(map(lambda name : m["species"][name]["num"],r["consumed"]))
            prod = list(map(lambda name: m["species"][name]["num"], r["produced"]))

            dd[r["num"]] = {'rate_constant' : float(rate_expr),'spec' : spec_single,'amt': amts, 'consumed' : cons, "produced": prod}
        with open('dynamic_rates.json', 'w') as outfile:
            json.dump(dd, outfile)

    # Write observations json
    obs_file = []
    for obs_name in m["observations"].keys():
        obs = {'name' : obs_name, 'vals' : list(map(lambda n: m["species"][n]['num'],m["observations"][obs_name])) }
        obs_file.append(obs)
    with open('observations_def.json', 'w') as outfile:
        json.dump(obs_file, outfile)

    if target == "rust":
        check_rust()
        generate_rust_code(m)
        stop_compile = timer()
        start_run = timer()
        print("Running generated rust code")
        if (not only_generate):
            out = subprocess.check_output("cargo run --manifest-path=generated_code/Cargo.toml --release", shell=True,stderr=subprocess.STDOUT)
        else:
            return
        #out = subprocess.check_output("generated_code/target/release/run_model", shell=True, stderr=subprocess.STDOUT)
        stop_run = timer();
        prop_time = out.decode("utf-8").split("Steps in ")[1].split(" -> ")[0]
        reported_time = float(prop_time)
        reported_steps = int(out.decode("utf-8").split("# --- ")[1].split("Steps in")[0])
        if print_output:
            print(out.decode("utf-8"))

    elif target == "python":
        #for r in m["rules"]:
        #    r["rate_expression"] = r["rate_expression"].replace("__species__:","self.")
        with open('templates/python.jinja') as file_:
            template_write_counts = jinja2.Template(file_.read())
            with open('python_generated.py', 'w') as f:
                py_code = template_write_counts.render(model=m)
                py_code = py_code.replace("__species__:","self.")
                py_code = py_code.replace("__float_constant__:", "self.")
                py_code = py_code.replace("__int_constant__:", "self.")
                f.write(py_code)

    elif target == "generic":
            check_rust()
            toml.dump(fix_numbers(m), open("temporary_model.toml", "w"))
            stop_compile = start_compile
            subprocess.check_output('cargo build --manifest-path=generic_simulator/Cargo.toml --release', shell=True,
                                    stderr=subprocess.STDOUT)

            start_run = timer()
            # out = subprocess.check_output('cargo run --manifest-path=parsed_runner/Cargo.toml --release', shell=True,stderr=subprocess.STDOUT)
            if (not only_generate):
                out = subprocess.check_output('generic_simulator/target/release/generic_simulator temporary_model.toml', shell=True,
                                          stderr=subprocess.STDOUT)
            else:
                return
            stop_run = timer()
            prop_time = out.decode("utf-8").split("Steps in ")[1].split(" s -> ")[0]
            reported_steps = int(out.decode("utf-8").split("# --- ")[1].split("Steps in")[0])
            reported_time = float(prop_time)
            if print_output:
                print(out)


    elif target == "gcc":
            try:
                subprocess.check_output('g++ -v', shell=True, stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError:
                raise Exception("gcc does not work. 'g++ -v' failed")
            if os.path.exists("prog_c.bin"):
                os.remove("prog_c.bin")
            generate_cpp_code(m, "g++",lto=lto,use_single_file=use_single_file)
            stop_compile = timer()
            start_run = timer()
            if (not only_generate):
                print("Starting Execution of model")
                out = ''
                pr = subprocess.Popen('./prog_c.bin', stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                for l in iter(lambda: pr.stdout.read(1), b''):
                    l = l.decode('utf-8')
                    print(l,end='')
                    out += l
                assert(pr.returncode is None)
                #out = subprocess.check_output('./prog_c.bin', shell=True)
            else:
                return
            stop_run = timer()
            prop_time = out.split("Steps in ")[1].split(" s -> ")[0]
            reported_time = float(prop_time)
            reported_steps = int(out.split("# -C- ")[1].split("Steps in")[0])
            if print_output:
                print(out)

    elif target == "clang":
            try:
                subprocess.check_output('g++ -v', shell=True, stderr=subprocess.STDOUT)
            except subprocess.CalledProcessError:
                raise Exception("gcc does not work. 'g++ -v' failed")
            if os.path.exists("prog_c.bin"):
                os.remove("prog_c.bin")
            generate_cpp_code(m, "clang++",lto=lto,use_single_file=use_single_file)
            stop_compile = timer()
            start_run = timer()
            if (not only_generate):
                out = subprocess.check_output('./prog_c.bin', shell=True)
            else:
                return
            stop_run = timer()
            prop_time = out.decode("utf-8").split("Steps in ")[1].split(" s -> ")[0]
            reported_time = float(prop_time)
            reported_steps = int(out.decode("utf-8").split("# -C- ")[1].split("Steps in")[0])
            if print_output:
                print(out)
    elif target == "bionetgen":
            if (extention != '.net'):
                raise Exception("Bionetgen target only supports .net files")
            stop_compile = start_compile
            bnglpath = get_bionetgen_path()


            start_run = timer()
            bng_cmd = bnglpath + " -o ./bngl_output -p ssa -h " + str(random.randint(10, 1000000000)) + " --cdat 1 --fdat 0 -g ./" + modelfile + " " + " ./" + modelfile + " " + str(
                m["until"] / m["num_steps"]) + " " + str(m["num_steps"])
            print(bng_cmd)
            if (not only_generate):
                out = subprocess.check_output(bng_cmd, shell=True)
            else:
                return
            stop_run = timer()
            prop_time = out.decode("utf-8").split("Propagation took")[1].split("CPU seconds")[0]
            reported_time = float(prop_time)
            reported_steps = int(out.decode("utf-8").split("TOTAL STEPS: ")[1].split("Time course of")[0])
            if print_output:
                print(str(out.decode("utf-8")))
    else:
            raise Exception("Unsupported target " + str(target))



    return {"t_parsing": stop_parse - start_parse, "t_sort": stop_sort - start_sort,
            "t_compile": stop_compile - start_compile, "t_total_run": stop_run - start_run, "t_reported": reported_time,
            "num_steps": reported_steps}


if __name__ == "__main__":
    # execute only if run as a script
    parser = argparse.ArgumentParser(description='Generate a specialized Simulator')
    parser.add_argument('model',help="the toml file of the model to use")
    parser.add_argument('--sort', help="Based on the previos execution, find optimal model order",action='store_true')
    parser.add_argument('--lto', help="Link time optimization, significantly faster code, but longer compilation for large models",action='store_true')
    parser.add_argument('--singlefile',
                        help="Write as single file",
                        action='store_true')
    parser.add_argument('--tau', help="Use Tau Leaping for fast reactions",action='store_true')
    parser.add_argument("--tree",
                        help="store reaction propensities in a tree structure",
                        action='store_true')
    parser.add_argument('--steps',type=int,help="The number of steps used for observation (default: 1000)",default=1000)
    parser.add_argument('--until',type=float,help="The time until to simulate (default: 10)",default=10)
    parser.add_argument("--target", help="name of the target(rust|generic|gcc|clang|python) (default: gcc)",default="gcc")
    parser.add_argument("--dry", help="only generate the needed files, but don't run the simulation", action='store_true')
    parser.add_argument("--dynamic_dep", help="don't hard code dependencies but read them dynamically", action='store_true')
    parser.add_argument("--dynamic_rate", help="don't hard code rates but read them dynamically (limited capability)",
                        action='store_true')
    parser.add_argument("--dynamic_exec", help="don't hard code execution but read them dynamically (limited capability)",
                        action='store_true')

    parser.add_argument("--machineoutput", help="write machine readable version of timing output",
                        action='store_true')

    parser.add_argument("--dontreplaceconstants", help="don't replace the constants in bionetgen files",
                        action='store_true')


    print_output = True



    args = parser.parse_args()

    res  = core_execution_function(args.model,args.sort,args.steps,args.until,args.target,tauleaping=args.tau,use_tree=args.tree,only_generate=args.dry,lto=args.lto,use_single_file=args.singlefile,use_dynamic_dependencies=args.dynamic_dep,use_dynamic_rates=args.dynamic_rate,use_dynamic_exec=args.dynamic_exec,replace_constants_in_bngl=not args.dontreplaceconstants)

    if not args.machineoutput:
        print("STEPS: " + str(args.steps))

        x = PrettyTable()
        x.field_names = ["m","val [s]"]
        for val in res:
            x.add_row([val,res[val]])

        print(x)
    else:
        print(res)