#include <iostream>
#include <fstream>
#include <limits>

#include "master_header.h"
#include "json.hpp"
using json = nlohmann::json;



void read_constants(){
    float_constants.fill(std::numeric_limits<double>::quiet_NaN());
    int_constants.fill(-1);
    std::ifstream config_file("constants.json");
    assert(config_file.good());
    nlohmann::json j;
    config_file >> j;
    {% for c in model["float_constants"].keys() -%}
    float_constants[{{model["float_constants"][c]["num"]}}] = j["float_constants"]["{{c}}"];
    {% endfor -%}

        {% for c in model["int_constants"].keys() -%}
    int_constants[{{model["int_constants"][c]["num"]}}] = j["int_constants"]["{{c}}"];
    {% endfor -%}
}

