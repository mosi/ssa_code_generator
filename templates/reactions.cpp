#include "master_header.h"
#include <iostream>
#include <cassert>

{% if not model.dynamic_rates %}

double my_propensity_{{-rule["uid"]-}}(){
    {% if rule["guards"] | length > 0%}
        if (false
        {%- for g in rule["guards"] %}
            || (! ( {{g.left}} {{g.compOp}} {{g.right}})) //
        {% endfor -%}
        )
        {
            return 0;
        } else
    {% endif %}
        {
            // Branch for guards ok
            return {{rule["rate_expression"]}};
        }
}


void update_rule_{{-rule["uid"]-}}(){

        {% if model.tau %}
        if(reaction_is_tau[{{-rule["num"]-}}]){
            propensity[{{rule["num"]}}] = my_propensity_{{-rule["uid"]-}}();
            return;
        }
        {% endif %}

        {% if model["linear"] %}
        propensity_sum  -= propensity[{{rule["num"]}}];
        {% endif %}



        propensity[{{rule["num"]}}] = my_propensity_{{-rule["uid"]-}}();

            {% if model.log >= 1 %}
                assert(propensity[{{rule["num"]}}] >= 0.0);
            {% endif %}
            {% if model["linear"] %}
            propensity_sum  += propensity[{{rule["num"]}}];
            {% else %}
                propensity_tree[{{rule["num"]}}] = propensity[{{rule["num"]}}];
                {% if 2*rule["num"]+1 < model.rules | length %}
                propensity_tree[{{rule["num"]}}] += propensity_tree[{{ 2*rule["num"]+1 }}];
                {% endif %}
                {% if 2*rule["num"]+2 <  model.rules | length %}
                propensity_tree[{{rule["num"]}}] += propensity_tree[{{ 2*rule["num"]+2 }}];
                {% endif %}


                /* first layer needs check */
                const auto tmp = ({{rule["num"]}}-1)/2;
                propensity_tree[tmp] = propensity_tree.at((2*tmp)+1) + propensity[tmp];
                if (2*tmp + 2 < {{model["rules"]|length }}){
                    propensity_tree[tmp] += propensity_tree.at((2*tmp)+2) ;
                }

                /* middle layers in loop */
                for( int i = (tmp-/*i*/1)/2; i >0; i = (i-1)/2){
                    propensity_tree[i] = propensity_tree.at((2*i)+1) + propensity_tree.at((2*i)+2) + propensity[i];
                }
                /* last layer explicitly */
                {% if rule["num"] != 0 %}
                     propensity_tree[0] = propensity_tree[1] + propensity_tree[2] + propensity[0];
                {% endif %}

            {% endif %}
        }
{% endif %}
{% if not model.dynamic_dependencies %}

    void update_affected_{{-rule["uid"]}}(){
         // Update Propensities
         {% for affr in rule["affected_rules"] -%}
           update_rule_{{-affr-}}();
         {% endfor %}
    }
{% endif %}

{% if model.tau %}
void execute_rule_{{-rule["uid"]-}}_multiple_times(uint n){
    assert(n > 0);
        {%- if model.count_rules -%}
            reaction_exec_counts[{{rule["num"]}}] += n;
    {%- endif -%}

    {%- for elem in rule["consumed"] %}
        populations[{{model.species[elem].num}}] /* {{model.species[elem].name}} */ -=n;
        {% if model.tau %}
        assert(populations[{{model.species[elem].num}}] > tau_limit / 2);
        {% endif %}
    {%- endfor -%}
    {%- for elem in rule["produced"] %}
        populations[{{model.species[elem].num}}] /* {{model.species[elem].name}} */ +=n;
    {%- endfor %}
}
{% endif %}


{% if not model.dynamic_execution %}
/* {{rule["description"]}}*/
void execute_rule_{{-rule["uid"]-}}(){
    {% if model.log >= 1 %}  assert(propensity[{{rule.num}}] > 0); {% endif %}
    {%- if model.count_rules -%}
            reaction_exec_counts[{{rule["num"]}}] += 1;
    {%- endif -%}

    {% if model.log >= 3 %}
        std::cout<<"reaction {{-rule["uid"]-}}"
        {%- for elem in rule["consumed"] %}
            <<" {{model.species[elem].name}} - 1 = "<< populations[{{model.species[elem].num}}] -1
        {%- endfor -%}
        {%- for elem in rule["produced"] %}
            <<" {{model.species[elem].name}} + 1 = "<< populations[{{model.species[elem].num}}] +1
        {%- endfor %}
        <<"\n";
    {% endif %}


    // Update state
    {%- for elem in rule["consumed"] %}
        populations[{{model.species[elem].num}}] /* {{model.species[elem].name}} */ -=1;
    {%- endfor -%}
    {%- for elem in rule["produced"] %}
        populations[{{model.species[elem].num}}] /* {{model.species[elem].name}} */ +=1;
    {%- endfor %}

    {%if not model.dynamic_dependencies %}
    update_affected_{{-rule["uid"]}}();
    {% else %}
    update_affected_rule_x({{-rule.num}});
    {% endif %}
}

{% endif %}

