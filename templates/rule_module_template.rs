
use crate::ModelStruct;


{% for rule in model["rules"] -%}
/* {{rule["description"]}}*/
pub fn update_rule_{{-rule["uid"]-}}(model : &mut ModelStruct){
        model.propensity_sum  -= model.propensity[{{rule["num"]}}];

        {%- for g in rule["guards"] -%}
        if ! (model.populations[{{model.species[g.species].num}}] /* {{model.species[g.species].name}} */ {{g["compOp"]}} {{g["num"]}})
        {
            model.propensity[{{rule["num"]}}] = 0.0;
            return;
        }
        {%- endfor -%}

        model.propensity[{{rule["num"]}}] = {{rule["rate_expression"]}} ;
        model.propensity_sum  += model.propensity[{{rule["num"]}}];
        }
{% endfor -%}

{% for rule in model["rules"] -%}
/* {{rule["description"]}}*/
pub fn execute_rule_{{-rule["uid"]-}}(model:&mut  ModelStruct){
    {%- if model.count_rules -%}
            model.reaction_exec_counts[{{rule["num"]}}] += 1;
    {%- endif -%}


    // Update state
    {%- for elem in rule["consumed"] %}
        model.populations[{{model.species[elem].num}}] /* {{model.species[elem].name}} */ -=1;
    {%- endfor -%}
    {%- for elem in rule["produced"] %}
        model.populations[{{model.species[elem].num}}] /* {{model.species[elem].name}} */ +=1;
    {%- endfor %}

    // Update Propensities
    {% for affr in rule["affected_rules"] -%}
       update_rule_{{-affr-}}(model);
    {%- endfor -%}
    }
{% endfor -%}