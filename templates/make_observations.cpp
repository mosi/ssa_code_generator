#include <iostream>
#include <fstream>
#include <cassert>
#include "ttimer.hpp"
#include "master_header.h"

void model_observe(size_t obs_num){
      for(size_t idx = 0; idx < observations_definitions.size(); idx++){
        observations[obs_num][idx] = 0;
        for(auto&k : observations_definitions[idx]){
            observations[obs_num][idx] += populations[k];
        }
      }

      {# {% for e in model["observations"].keys() -%}
            observations[obs_num][{{loop.index0}}] = 0
            {%- for n in model["observations"][e] -%}
                + populations[{{model.species[n].num}}]
            {%- endfor -%}
            ;
        {% endfor %} #}
}

