#include <cmath>
#include <random>
#include <iostream>
#include <chrono>
#include <fstream>
#include <algorithm>    // std::find
#include <stdexcept>
#include <cassert>
#include <iomanip>
#include "ttimer.hpp"
#include "pcg_random.hpp"
#include "cxxopts.hpp"


/* Progress Bar Code */
size_t total_num_ticks = 10000000;
namespace progresscpp {
class ProgressBar {
private:
    unsigned int ticks = 0;

    const size_t total_ticks = total_num_ticks;
    const size_t bar_width = 70;
    const char complete_char = '=';
    const char incomplete_char = ' ';
    const std::chrono::steady_clock::time_point start_time = std::chrono::steady_clock::now();
    mutable ttimer time_counter;

public:
    ProgressBar(){time_counter.start();}

    void set(size_t val) {
        ticks = val;
    }

    void display() const {
        if (time_counter.timePassed() > 0.01){
            time_counter.stop();
            time_counter.reset();
            time_counter.start();
            float progress = (float) ticks / total_ticks;
            int pos = (int) (bar_width * progress);

            std::chrono::steady_clock::time_point now = std::chrono::steady_clock::now();
            auto time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - start_time).count();

            std::cout << "[";

            for (int i = 0; i < bar_width; ++i) {
                if (i < pos) std::cout << complete_char;
                else if (i == pos) std::cout << ">";
                else std::cout << incomplete_char;
            }
            std::cout << "] " << std::fixed<<std::setw(5) <<std::setprecision(2) << progress * 100.0 << "% "
                      << float(time_elapsed) / 1000.0 << "s of "<<float(time_elapsed)/1000.0/progress<<"s\r";
            //std::cout.flush();
        }
    }

    void done() const {
        display();
        std::cout << std::endl;
    }
};
}
    progresscpp::ProgressBar bar;


#include "master_header.h"
//#include "json.hpp"
//using json = nlohmann::json;

std::random_device rd;
std::mt19937_64 gen(rd());
// std::minstd_rand gen(rd());

// Seed with a real random value, if available
//pcg_extras::seed_seq_from<std::random_device> seed_source;

    // Make a random number engine
 //   pcg32 gen(seed_source);

std::uniform_real_distribution<> dis(0, 1.0);
std::exponential_distribution<> d_exp(1);
double randomDouble() { return dis(gen); }

double exp_random(double rate) { return d_exp(gen) / rate; }


size_t observations[{{model["num_steps"]}}+1][{{ model["observations"] | length }}];

std::array<double,{{model["float_constants"]|length}}> float_constants;
std::array<int,{{model["int_constants"]|length}}> int_constants;

{% if model["species"]|length < 500 %}
std::array<size_t,{{model["species"]|length}}> populations;
{% else %}
std::vector<size_t> populations({{model["species"]|length}});
{% endif %}

{% if model["rules"]|length < 500 %}
 std::array<double,{{model["rules"]|length}}> propensity;
{% else %}
 std::vector<double> propensity({{model["rules"]|length}});
{% endif %}


{% if not model.linear %}
std::array<double,{{model["rules"]|length}}> propensity_tree;
{% endif %}


ttimer tinit;
ttimer tselect;
ttimer texecute;

std::string file_header = "";



{%- if model.count_rules -%}
std::array<size_t,{{model["rules"]|length}}> reaction_exec_counts;
{%- endif -%}

{% if model.dynamic_rates %}
 std::vector<std::vector<size_t>> reactions_spec_in_index;
 std::vector<std::vector<size_t>> reactions_spec_in_cardi;
 std::vector<float> reactions_constant;
void update_rule_x(size_t x){
    propensity_sum -= propensity[x];

    propensity[x] = reactions_constant[x];
    //std::cout<<" REAC "<<x<<" k= "<<reactions_constant.at(x);
    for (int k = 0; k < reactions_spec_in_cardi[x].size(); k++){
            auto idx = reactions_spec_in_index[x][k];
            auto amt = reactions_spec_in_cardi[x][k];
            //std::cout<<" #"<<amt<<"x"<<idx;
            if(amt > populations[idx]){
                propensity[x] = 0;
                //std::cout<<" Abort\n";
                return;
            }
            for (int count = 0; count < amt; count++){
                propensity[x] *= static_cast<double>(populations[idx]-count);
            }
            //std::cout<<" => Prop(x) = "<<propensity[x];
    }
    //std::cout<<"\n";

    propensity_sum += propensity[x];
    //std::cout<<"sum = "<<propensity_sum<<"\n";
}

{% elif model.dynamic_dependencies %}
void update_rule_x(size_t x){
    switch (x) {
    {% for r in model["rules"] %}
        case {{r.num}} :  {
                update_rule_{{r.uid}}();
        break;
        }
    {%- endfor -%}
    default: //panic!("Reaction selection Error!")
       throw std::runtime_error("Reaction Selection error!");
        break;
    }
}
{% endif %}

{% if model.dynamic_dependencies %}
std::vector<std::vector<size_t>> dependent_reactions;
void update_affected_rule_x(size_t x){
    for(auto& i : dependent_reactions[x]){
        update_rule_x(i);
    }
}
{% endif %}

{% if model.tau %}

size_t tau_occurences = 0;
size_t tau_skips = 0;

std::array<bool,{{model["rules"]|length}}> reaction_is_tau;
std::vector<size_t> idxes_of_tau_reactions;

void execute_rule_x_ntimes(size_t x, uint n){
    switch (x) {
    {% for r in model["rules"] %}
        case {{r.num}} :  {
                execute_rule_{{r.uid}}_multiple_times(n);
        break;
        }
    {%- endfor -%}
    default: //panic!("Reaction selection Error!")
        throw std::runtime_error("Reaction Selection error!");
        break;
    }
}

void update_affected_rule_x(size_t x){
    switch (x) {
    {% for r in model["rules"] %}
        case {{r.num}} :  {
                update_affected_{{r.uid}}();
        break;
        }
    {%- endfor -%}
    default: //panic!("Reaction selection Error!")
        throw std::runtime_error("Reaction Selection error!");
        break;
    }
}




double tau_step_part_1(double tau){
    if (tau > max_tau_time_step){
        tau = max_tau_time_step;
    }
    /* check for max propensity */
    double max_change = 0.0;
    for (auto i : idxes_of_tau_reactions){
        max_change = std::max(maximum_relative_change_per_execution(i)*propensity[i],max_change);
    }
    max_change *= tau;

    if(max_change > max_relative_change){
        //std::cout<<" reducing tau : "<<tau;
        tau = max_relative_change/ (max_change/tau);
        //std::cout<<" -> "<<tau<<"\n";
    }



    /* Perform tau update step */
    for (auto i : idxes_of_tau_reactions){
        if (propensity[i] == 0 ){
            continue;
        }
        const double p_val = propensity[i]*tau;

        std::poisson_distribution<int> d(p_val);
        const int reps = d(gen);

        if(reps > 0){
            tau_skips += reps;
            tau_occurences += 1;
            execute_rule_x_ntimes(i,reps);
        }
    }
    return tau;
}

void tau_step_part_2(){
    /* check if still "fast" */
    std::vector<size_t> idx_to_remove;
    for (auto i : idxes_of_tau_reactions){
        if (! reaction_is_tau_calc(i)){
            idx_to_remove.push_back(i);
        } else {
            update_affected_rule_x(i);
        }
    }
    /* and remove if not */
    const auto old_length = idxes_of_tau_reactions.size();
    //assert(idx_to_remove.size() == 0); // first test
    for (auto i : idx_to_remove){
        idxes_of_tau_reactions.erase(std::find(idxes_of_tau_reactions.begin(),idxes_of_tau_reactions.end(),i));
        assert(reaction_is_tau[i]);
        reaction_is_tau[i] = false;
        propensity[i] = 0;
        update_affected_rule_x(i);
    }
    assert(idxes_of_tau_reactions.size()  + idx_to_remove.size() == old_length);
}

{% endif %}






{% if model.linear %}
double    propensity_sum =0.0;
{% endif %}
double    simtime = 0.0;
double target_time = -1;
size_t  step_counter =0;


void step(){
        step_counter += 1;


        if (step_counter % {{model.rules | length }} == 0){
          init_all_propensity();
        }

        if ( {% if model.linear %} propensity_sum {% else %} propensity_tree[0] {% endif %} <= 0.0 {% if model.tau %} && idxes_of_tau_reactions.empty() {% endif %}) {
            std::cout<<"No reaction possible -> early termination\n";
            simtime = target_time;
            return;
            //throw std::runtime_error("No reaction possible!");
        }

        if ( {% if model.linear %} propensity_sum {% else %} propensity_tree[0] {% endif %} <= 1./(10.*target_time) {% if model.tau %} && idxes_of_tau_reactions.empty() {% endif %}) {
            std::cout<<"Very low reaction propensity (numerical error?) -> early termination\n";
            simtime = target_time;
            return;
            //throw std::runtime_error("No reaction possible!");
        }

        const double timestep = exp_random({% if model.linear %} propensity_sum {% else %} propensity_tree[0] {% endif %});
        {% if model.tau %}

        if( (! idxes_of_tau_reactions.empty() ) ){
            double new_timestep = tau_step_part_1(timestep);
            if(new_timestep < timestep){
                simtime += new_timestep;
                tau_step_part_2();
                return; // only tau, no regular steps
            }
        }
        {% endif %}

        simtime += timestep;



        double max = randomDouble()*{% if model.linear %} propensity_sum {% else %} propensity_tree[0] {% endif %};

        {% if model.tau %}
        if ( max <= 0){
            // no none tau reaction possible
            tau_step_part_2();
            return;
        }
        {% endif %}

        //tselect.start();
          size_t x = 0;


          {% if model.linear %}
              double sum = 0.0;
              for (x=0;sum <= max; x++){
                  {% if model.tau %}
                  if (!reaction_is_tau[x]){
                    sum += propensity[x];
                  }
                  {% else %}
                  sum += propensity[x];
                  {% endif %}
              }
              x -= 1;
           {% else %}
           /*for (auto v : propensity_tree){ std::cout<<v<<" ";}
           std::cout<<"\n";
           for (auto v : propensity){ std::cout<<v<<" -";}
           std::cout<<"\n";*/
              while ( propensity[x] <= max){
              //assert(max <= propensity_tree[x]);
                max -= propensity[x];
                if ( (2*x)+1 < {{model["rules"]|length}}){
                }
                if( propensity_tree[(2*x)+1] >= max ){
                    x = (2*x)+1;
                } else {
                    max -= propensity_tree[(2*x)+1];
                    x = (2*x)+2;
                }
                //assert(max > 0);
                //  assert(x < {{  model["rules"] | length }} );
              }
          {% endif %}
         //tselect.stop();


           {% if model.dynamic_execution %}
           {% if model.log >= 1 %}  assert(propensity[{{rule.num}}] > 0); {% endif %}
                {%- if model.count_rules -%}
                        reaction_exec_counts[x] += 1;
                {%- endif -%}

                // Update state
                for(auto & cons : consumed_data[x]){
                    populations[cons]  -=1;
                }
                for(auto & prod : produced_data[x]){
                    populations[prod]  +=1;
                }

                {%if not model.dynamic_dependencies %}
                static_assert(false); // not possible
                update_affected_();
                {% else %}
                update_affected_rule_x(x);
                {% endif %}
           {% else %}
         //texecute.start();
            switch (x) {
            {% for r in model["rules"] %}
                case {{r.num}} :  {
                        execute_rule_{{r.uid}}();
                break;
                }
            {%- endfor -%}
            default: //panic!("Reaction selection Error!")
                throw std::runtime_error("Reaction Selection error!");
                break;
            }
            {% endif %}

        {% if model.tau%}
        tau_step_part_2();
        {% endif %}
//texecute.stop();
}







void run_until(double target_time){
    size_t count = 0;
    while (simtime < target_time){
        count+= 1;
        if (count % 500000 == 0){
            count =0;
            bar.set(simtime / float(target_time) * total_num_ticks);
            bar.display();
        }
        if (count % 1000000 == 0){
            write_reaction_counts("reaction_counts_partial.toml");
        }
        step();
    }
}





int main(int argc, char** argv) {
    tinit.start();
    read_constants();
    initilize_values();

    cxxopts::Options options("CustomRulesSimulator", "A custom simulator generated based on a rule model");

    options.add_options()
      ("prefix", "output file name prefix", cxxopts::value<std::string>()->default_value(""))
      ("t,time", "time until to run simulation", cxxopts::value<double>()->default_value("{{model["until"]}}"))
     ;
     auto result = options.parse(argc, argv);

    target_time = result["time"].as<double>();

    const double delta_t = target_time * 1.0 / {{model["num_steps"]}};


    init_all_propensity();


    auto start = std::chrono::high_resolution_clock::now();
    //print_first_line();
    model_observe(0);

    tinit.stop();


    for(int c = 1; c < {{model["num_steps"]}}; c++){
        bar.display();
        run_until(c * delta_t);
        bar.set(c*delta_t * 1.0 / float(target_time) * total_num_ticks);
        model_observe(c);
    }
    bar.done();
    run_until(target_time);
    model_observe({{model["num_steps"]}});
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;

    {
        std::ofstream myfile;
        myfile.open (result["prefix"].as<std::string>()+"cpp_observations.txt");
        myfile << file_header;
        for (int line = 0; line <= {{model["num_steps"]}}; line++ ){
            myfile<< delta_t *line<<",";
            for (auto& k : observations[line]){
                myfile <<k<<",";
            }
            myfile<<"\n";
        }
        myfile.close();
    }

    write_reaction_counts();




    std::cout<<"TIMINGS: init="<<tinit.timeInSec()<<"  select="<<tselect.timeInSec()<<"  exec="<<texecute.timeInSec()<<"\n";
    {% if model.tau %}
    std::cout<<" TAU: skips: "<<tau_skips<<"(" << tau_skips*1.0/step_counter<<"%) - occur: "<<tau_occurences<<"("<<tau_occurences*1.0/step_counter<<"%)\n";
    {% endif %}

    std::cout<< "# -C- "<<step_counter<<" Steps in "<<elapsed.count() << " s -> "<<step_counter/(elapsed.count() *1e6) <<" steps per mu_sec\n";

    return 0;
}


