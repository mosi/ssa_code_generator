#include "master_header.h"
#include <cassert>
#include <iostream>

#include "json.hpp"
using json = nlohmann::json;
#include <string>
#include <fstream>
#include <streambuf>

{% if model.tau  %}

std::vector<std::vector<size_t>> consumed_data = {
{% for rule in model.rules %}
    {
    {%- for elem in rule["consumed"] -%}
        {{model.species[elem].num}},
    {%- endfor -%}
    },
{%- endfor %}
};
std::vector<std::vector<size_t>> produced_data{
{% for rule in model.rules %}
    {
    {%- for elem in rule["produced"] -%}
        {{model.species[elem].num}},
    {%- endfor -%}
    },
{%- endfor %}
};


void check_all_reactions_if_tau(){
    idxes_of_tau_reactions.clear();
    for(size_t  i = 0; i < {{model["rules"] | length}}; i++){
        reaction_is_tau[i] = reaction_is_tau_calc(i);
        if(reaction_is_tau[i]){
        idxes_of_tau_reactions.push_back(i);
        }
    }
}
{% endif %}

void init_all_propensity(){
     std::fill(propensity.begin(),propensity.end(),0.0);
     {% if model.tau %}
     std::fill(reaction_is_tau.begin(),reaction_is_tau.end(),false);
     {% endif %}

    {% if not model.linear %}
    std::fill(propensity_tree.begin(),propensity_tree.end(),0);
    {% else %}
    propensity_sum = 0.0;
    {% endif %}

    {% if model.tau %}
     check_all_reactions_if_tau();
    {% endif %}
    {% if model.dynamic_rates %}
    for(int i = 0; i < {{model.rules | length}}; i++){
        update_rule_x(i);
    }
    {%endif %}
    {% for rule in model["rules"] -%}
        {% if model.dynamic_rates %}
        {% else %}
        update_rule_{{-rule["uid"]-}}();
        {% endif %}
        {% if model.tau %}

        {% endif %}
        {#/*if(propensity[{{-rule["num"]-}}] < 0){
            std::cout<<"Error: The initial propensity of a reaction is negative! Are all constants set?\n"
            <<"{{rule["description"]}}"<<"\n";
        }*/
        //assert(propensity[{{-rule["num"]-}}] >= 0); // {{rule["description"]}}#}
    {% endfor -%}
    for (auto& p : propensity){
        assert(p>=0.0);
    }
}

std::vector<std::string> reaction_names;

{% if model.dynamic_execution %}
std::vector<std::vector<size_t>> consumed_data;
std::vector<std::vector<size_t>> produced_data;
{% endif %}

std::vector<std::vector<size_t>> observations_definitions;

void initilize_values(){
    file_header += "time,";
       {
        std::ifstream i("observations_def.json");
         json j;
         i >> j;
         assert(j.is_array());
         observations_definitions.reserve(j.size());
        for (auto &r : j){
         observations_definitions.push_back(r["vals"]);
         file_header+=r["name"];

        }

    }
    {#{%- for e in model["observations"].keys() -%}{{-e-}},{%- endfor -%}\n";;#}

    std::fill(populations.begin(),populations.end(),0);
    std::fill(propensity.begin(),propensity.end(),0.0);

    {% if not model.linear %}
    std::fill(propensity_tree.begin(),propensity_tree.end(),0);
    {% endif %}

    {%- if model.count_rules -%}
    std::fill(reaction_exec_counts.begin(),reaction_exec_counts.end(), 0);
    {%- endif -%}

    {%- for e in model["species"].values() -%}
    {% if e.initial != 0 and e.initial != "0"%}
     populations[{{e.num}}]= {{e.initial}} /* {{e.name}} */;
     assert({{e.initial}} >= 0);
    {#if( {{e.initial}} < 0){
        std::cout<<"Error: The initial amount of a reactant is negative! Are all constants set?\n"
        <<"Amount of {{e.name}} is = "<<{{e.initial}}<<"\n";
    }#}
     assert({{e.initial}} >= 0);
     {% endif %}
     {%- endfor -%}

     {%- if model.count_rules -%}
        {
        std::ifstream i("reaction_model_parsed.json");
         json j;
         i >> j;
         reaction_names.reserve(j["rules"].size());
        for (auto &r : j["rules"]){
         reaction_names.push_back(r["uid"]);
        }
        }
    {%- endif -%}



     {% if model.dynamic_dependencies %}
     {
         std::ifstream i("dynamic_dependencies.json");
         json j;
         i >> j;
         assert(j.is_array());
         dependent_reactions.resize(j.size());
         for(int i = 0; i < j.size(); i++){
            assert(j[i].is_array());
            for(int k = 0; k < j[i].size(); k++){
              dependent_reactions[i].push_back(j[i][k]);
            }
         }
     }
     //dependent_reactions
     {% endif %}



     {% if model.dynamic_rates  or model.dynamic_execution %}
     {
         std::ifstream i("dynamic_rates.json");
         json j;
         i >> j;

        assert(j.is_array());
        for (int i = 0; i < j.size(); i++) {
           {% if model.dynamic_rates %}
              reactions_constant.push_back(j[i]["rate_constant"]);
              reactions_spec_in_index.push_back(j[i]["spec"]);
              reactions_spec_in_cardi.push_back(j[i]["amt"]);
            {% endif %}
            {% if model.dynamic_execution %}
               consumed_data.push_back(j[i]["consumed"]);
               produced_data.push_back(j[i]["produced"]);
           {% endif %}
        }

         /* std::vector<std::vector<size_t>> reactions_spec_in_index;
          std::vector<std::vector<size_t>> reactions_spec_in_cardi;
           std::vector<float> reactions_constant;*/

       }
     {% endif %}
}

