
/* master header */

#ifndef master_header_hpp
#define master_header_hpp

#include <stddef.h>
#include <array>
#include <cmath>
#include <string>
#include <vector>
#include <cassert>

extern size_t observations[][{{model["species"]|length}}];

{% if model["rules"]|length < 500 %}
extern std::array<double,{{model["rules"]|length}}> propensity;
{% else %}
extern std::vector<double> propensity;
{% endif %}

{%- if not model.linear%}
extern std::array<double,{{model["rules"]|length}}> propensity_tree;
{% endif %}

extern std::array<double,{{model["float_constants"]|length}}> float_constants;
extern std::array<int,{{model["int_constants"]|length}}> int_constants;


{% if model["species"]|length < 500 %}
extern std::array<size_t,{{model["species"]|length}}> populations;
{% else %}
extern std::vector<size_t> populations;
{% endif %}

{% if model.linear %}
extern double propensity_sum;
{% endif %}

{%- if model.count_rules -%}
extern std::array<size_t,{{model["rules"]|length}}> reaction_exec_counts;
{%- endif -%}

void read_constants();

{% if not model.dynamic_rates %}
{% for rule in model["rules"] -%}
void update_rule_{{-rule["uid"]-}}();
    {% if model.tau %}
    void execute_rule_{{-rule["uid"]-}}_multiple_times(uint n);
    void update_affected_{{-rule["uid"]}}();
    {% endif %}
{% endfor -%}
{% endif %}

void update_affected_rule_x(size_t x);

void init_all_propensity();

void initilize_values();

{% if not model.dynamic_execution %}
{% for r in model["rules"] -%}
void execute_rule_{{-r["uid"]-}}();
{% endfor -%}
{% endif %}


inline int __my_rounding_function(double v){
    return static_cast<int>( std::round(v));
}

{% if model.tau or model.dynamic_execution %}
extern std::vector<std::vector<size_t>> consumed_data;
extern std::vector<std::vector<size_t>> produced_data;
{% endif %}

{% if model.tau %}


void check_all_reactions_if_tau();

extern std::vector<size_t> idxes_of_tau_reactions;
const size_t tau_limit = 1000;
const double max_relative_change = 0.1;
const double max_tau_time_step = 1;
extern std::array<bool,{{model["rules"]|length}}> reaction_is_tau;

inline bool reaction_is_tau_calc(size_t n){
    for( auto k: consumed_data[n]){
        if (populations[k] < tau_limit){
            return false;
        }
    }
    for( auto k: produced_data[n]){
        if (populations[k] < tau_limit){
            return false;
        }
    }
    return true;
}

inline double maximum_relative_change_per_execution(size_t n){
    double res = 1.0;
    for( auto k: consumed_data[n]){
        res = std::max(  populations[k] / (populations[k] - 1.0)  ,res);
    }
    for( auto k: produced_data[n]){
        res = std::max(  (populations[k] + 1.0)  / populations[k]   ,res);
    }
    return res - 1;
}
{% endif %}

void write_reaction_counts( std::string file_name = "reaction_counts.toml" );
void model_observe(size_t obs_num);
extern std::string file_header;

{% if model.dynamic_dependencies %}
/* map reaction -> affected reaction */
extern std::vector<std::vector<size_t>> dependent_reactions;
void update_rule_x(size_t x);
{% endif %}


{% if model.dynamic_dependencies %}
/* map reaction -> affected reaction */
extern std::vector<std::vector<size_t>> dependent_reactions;
void update_rule_x(size_t x);
void update_affected_rule_x(size_t x);
{% endif %}

{% if model.dynamic_rates %}
void update_rule_x(size_t x);
/* map reaction -> affected reaction */
extern std::vector<std::vector<size_t>> reactions_spec_in_index;
extern std::vector<std::vector<size_t>> reactions_spec_in_cardi;
extern std::vector<float> reactions_constant;
{% endif %}

extern std::vector<std::string> reaction_names;
extern std::vector<std::vector<size_t>> observations_definitions;

#endif

