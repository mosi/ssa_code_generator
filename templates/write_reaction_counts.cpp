#include <iostream>
#include <fstream>
#include <cassert>
#include "ttimer.hpp"
#include "master_header.h"


void write_reaction_counts(std::string file_name ){
    {%- if model.count_rules -%}

    std::ofstream myfile;
    myfile.open(file_name);
    for( size_t idx = 0; idx < reaction_exec_counts.size(); idx++){
        myfile << reaction_names[idx]<<" = "<<reaction_exec_counts[idx]<<"\n";
    }
    {#
    {% for r in model["rules"] %}
    myfile << "{{r["uid"]}} = "<< reaction_exec_counts[{{r["num"]}}];
    myfile << " # {{r["description"]}}"<<"\n";
    {% endfor %}
    myfile.close();
    #}


    {%- endif -%}
}
