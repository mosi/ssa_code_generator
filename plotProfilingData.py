#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

def convergences(filename):
    df_in = pd.read_csv(filename)

    df = pd.DataFrame()

    df["total step"] = df_in['steps Mean point_estimate'] / df_in['steps']
    df["step + select"] = df_in['steps + select Mean point_estimate'] / df_in['steps']
    df["step + rand"] = df_in['steps + rnd Mean point_estimate'] / df_in['steps']
    df["replay select exec"] = df_in['select+execute replay2 Mean point_estimate'] / df_in['steps']
    df["replay exec"] = df_in['execute replay Mean point_estimate'] / df_in['steps']
    df["rand only"] = df_in['rand only Mean point_estimate']

    df["model"] = df_in["model"]
    df["steps"] = df_in["steps"]
    df["simulator"] = df_in["guided opt"].apply(lambda x: "sorted " if x else "") + df_in["simulator"]
    df["simulator"] = df_in["simulator"]
    # df["guided opt"] = df_in["guided opt"]

    # steps = list(sorted(set(df["steps"][df["steps"].notnull()])))
    models = list(set(df["model"]))
    simulators = set(df["simulator"])

    for m in models:
        if not "state" in m:
            continue
        dfgrp_tot = df.groupby(["simulator", "steps", "model"]).mean()

        mstr = m.replace('models/', '').replace('networks/', '').replace('.net', '').replace('2', '')
        for s in simulators:
            try:
                # if not "compiled" in s:
                #    continue
                plt.title(mstr + " : " + s)

                # print(dfgrp.xs('compiled',level='simulator'))

                dfgrp = dfgrp_tot.xs(s, level='simulator').xs(m, level='model')

                dfgrp["rnd1"] = dfgrp["step + rand"] - dfgrp["total step"]
                dfgrp["select1"] = dfgrp["step + select"] - dfgrp["total step"] - dfgrp["rnd1"]
                dfgrp["exec1"] = dfgrp["total step"] - dfgrp["rnd1"] - dfgrp["select1"]

                plt.xscale("log")
                plt.fill_between(dfgrp.index, dfgrp["rnd1"], label="random")
                plt.fill_between(dfgrp.index, dfgrp["select1"] + dfgrp["rnd1"], dfgrp["rnd1"], label="select")
                plt.fill_between(dfgrp.index, dfgrp["exec1"] + dfgrp["rnd1"] + dfgrp["select1"],
                                 dfgrp["rnd1"] + dfgrp["select1"], label="execute")
                plt.legend()
                plt.xlabel('number of steps')
                plt.ylabel('time spent per iteration [ns]')

                plt.gcf().set_size_inches(6, 4, forward=True)
                plt.tight_layout()
                plt.savefig('convergence_' + mstr + '_' + s.replace(' ', '_') + '.pdf')
                plt.clf()
            except:
                plt.clf()
                print("WARINGING: Some convergence plot was not possible (incomplete data?) "+ s +" " +m)

            #plt.show()

def plotProfilingData(filename):

    df = pd.read_csv(filename)


    df["model"] = df["model"].apply(
        lambda x: x.replace('networks/', '').replace('models/', '').replace('.reaction_model', '').replace('_', '').replace(
            'gen', '').replace('predPrey', 'Lotka-Voltera').replace('site2', 'site'))
    df["model"] = df["model"].apply(
        lambda x: x.replace('networks/', '').replace('multistate', 'Multistate').replace('multisite', 'Multisite').replace(
            'egfrnet', 'EGFR').replace('.net', ''))

    df = df[df["model"].apply(lambda x: not 'beta' in x)]

    df["simulator"] = df["simulator"].apply(lambda x: x.replace('parsed', 'generic').replace('compiled', 'specialized').replace('rust','specialized'))

    models = set(df['model'])
    num_of_models = len(models)

    stepmax = np.max(df['steps'])

    set_width = .35

    printer = pd.DataFrame()


    def first_digit_is_1(val):
        v2 = val
        while (v2 < 10):
            v2 *= 100
        return str(v2)[0] == '1'


    def print_number_w_error(val, err):
        if val < .001:
            return ""

        if(np.isnan(err)):
            err = val
            print("WARING: Too few runs to determine error. Therefore error is value")

        num_err = 1
        if first_digit_is_1(err):
            num_err = 2

        while float(np.format_float_positional(err, precision=num_err, unique=False, fractional=False, trim='k')) < err:
            err *= 1.001

        err = np.format_float_positional(err, precision=num_err, unique=False, fractional=False, trim='k')

        num_err = 1
        if first_digit_is_1(float(err)):
            num_err = 2

        err2 = np.format_float_positional(float(err), precision=num_err, unique=False, fractional=False, trim='k').rstrip(
            '.')
        err = float(err2)

        num_signif = str(max(int(np.floor(np.log10(val)) - np.floor(np.log10(err))) + num_err, 1))
        #print(num_signif)
        # num_signif = str(2)

        err2 = ("{:#." + str(num_err) + "G}").format(err).rstrip(".")

        return str("{:#." + num_signif + "G}").format(val).rstrip(".") + " $ \pm $ " + err2


    def get_merged(A, B, name):
        d = pd.DataFrame()
        d['one'] = A[name]
        d['two'] = B[name]

        return d.apply(lambda x: print_number_w_error(x['one'], x['two']), axis=1)


    printer = pd.DataFrame()
    df_extra = df.copy()

    relavant_keys = ['steps Mean point_estimate', 'steps + select Mean point_estimate',
                     'select+execute replay2 Mean point_estimate', 'steps + rnd Mean point_estimate',
                     'execute replay Mean point_estimate']

    for k in relavant_keys:  # ['steps Mean point_estimate','steps + select Mean point_estimate','select+execute replay2 Mean point_estimate','steps + rnd Mean point_estimate','execute replay Mean point_estimate']:
        df_extra[k] = df_extra[k] / df_extra['steps']
        k2 = k.replace('point_estimate', 'standard_error')
        df_extra[k2] = df_extra[k2] / df_extra['steps']

    df_extra['simulator'] = df_extra['simulator'].apply(lambda x: x.replace('sorted', 'ODM'))

    printer_mean = df_extra[relavant_keys + ['model', 'simulator', 'steps', 'rand only Mean point_estimate']].groupby(
        ['model', 'simulator', 'steps']).mean()
    printer_sem = df_extra[relavant_keys + ['model', 'simulator', 'steps', 'rand only Mean point_estimate']].groupby(
        ['model', 'simulator', 'steps']).sem()

    printer_mean['total'] = printer_mean['steps Mean point_estimate']
    printer_sem['total'] = printer_sem['steps Mean point_estimate']

    printer_mean['rand2'] = printer_mean['rand only Mean point_estimate']
    printer_sem['rand2'] = printer_sem['rand only Mean point_estimate']

    printer_mean['rand1'] = printer_mean['steps + rnd Mean point_estimate'] - printer_mean['steps Mean point_estimate']
    printer_sem['rand1'] = printer_sem['steps + rnd Mean point_estimate'] + printer_sem['steps Mean point_estimate']

    printer_mean['select1'] = printer_mean['steps + select Mean point_estimate'] - printer_mean[
        'steps Mean point_estimate'] - printer_mean['rand1']
    printer_sem['select1'] = printer_sem['steps + select Mean point_estimate'] + printer_sem['steps Mean point_estimate'] + \
                             printer_sem['rand1']

    printer_mean['exec1'] = printer_mean['total'] - printer_mean['select1'] - printer_mean['rand1']
    printer_sem['exec1'] = printer_sem['total'] + printer_sem['select1'] + printer_sem['rand1']

    # printer_sem = df_extra.groupby(['model','simulator','steps']).sem()
    # printer_sem = df.groupby(['model','simulator','guided opt']).sem()

    # printer['total'] = get_merged(printer_mean,printer_sem,'steps Mean point_estimate')
    printer['random [ns]'] = get_merged(printer_mean, printer_sem, 'rand1')
    printer['select [ns]'] = get_merged(printer_mean, printer_sem, 'select1')
    printer['execute [ns]'] = get_merged(printer_mean, printer_sem, 'exec1')
    # printer['step time [ns]'] = printer_mean['steps Mean point_estimate']

    # printer['regular step'] = get_merged(printer_mean,printer_sem,'steps Mean point_estimate')
    # printer['sorting'] = get_merged(printer_mean,printer_sem,'t_sort')



    table = printer[printer.index.get_level_values('steps') == 1024].to_latex(index=True, escape=False)
    with open("profilingData_table.txt", 'w') as f:
        f.write(table)
    print(table)



    def sort_func(x):
        res = 0
        if "sorted" in x:
            res += .5
        if "specialized" in x:
            res += 1
        return res


    def sort_func2(x):
        res = 0

        if "otka" in x:
            return -3
        if "ultistate" in x:
            return -2
        if "ultisite" in x:
            return -1

        return res


    for mod_num, model in enumerate(sorted(models, key=sort_func2)):
        df2 = df[df['model'] == model]
        sims = list(sorted(set(df2['simulator']), key=sort_func))
        # sims = set(filter(lambda x: not "compiled" in x,sims))

        # steptimes = []
        rand_only = []
        select_only = []
        total_step = []
        step_select = []
        step_rand = []
        select_execute_replay = []
        execute_replay = []

        scaling = 1

        for s in sims:
            df3 = df2[df2['simulator'] == s]

            # print(s)
            rand_only.append(df3['rand only Mean point_estimate'].mean())
            total_step.append(((df3['steps Mean point_estimate'] / df3['steps'])[df3['steps'] == stepmax]).mean())
            step_select.append(((df3['steps + select Mean point_estimate'] / df3['steps'])[df3['steps'] == stepmax]).mean())
            select_execute_replay.append(
                ((df3['select+execute replay2 Mean point_estimate'] / df3['steps'])[df3['steps'] == stepmax]).mean())
            step_rand.append(((df3['steps + rnd Mean point_estimate'] / df3['steps'])[df3['steps'] == stepmax]).mean())
            execute_replay.append(
                ((df3['execute replay Mean point_estimate'] / df3['steps'])[df3['steps'] == stepmax]).mean())
            select_only.append(df3['select only Mean point_estimate'].mean())

        # steptimes = np.array(steptimes)
        rand_only = np.array(rand_only) * scaling
        select_only = np.array(select_only) * scaling
        total_step = np.array(total_step) * scaling
        step_select = np.array(step_select) * scaling
        step_rand = np.array(step_rand) * scaling
        select_execute_replay = np.array(select_execute_replay) * scaling
        execute_replay = np.array(execute_replay) * scaling

        max_time = np.amax(total_step)

        plt.subplot(1, num_of_models, mod_num + 1)
        plt.ylim(0, max_time * 1.3)
        plt.ylabel('time spent per step [ns]')
        plt.title(model.replace("models/", "").replace(".net", ""))

        ind = np.arange(len(sims))

        plotwidth = .4

        rnd2 = step_rand - total_step
        select2 = step_select - total_step - rnd2
        exec2 = total_step - rnd2 - select2

        (locs, lab) = plt.xticks(ind, list(map(lambda x: x.replace('sorted ', 'ODM-\n'), sims)),
                                 rotation=70)  # ,horizontalalignment="right")
        for l in lab:
            # print(l.get_position)
            # Todo fix horizontal allignment
            l.set_x(l.get_position()[1] - 100)

        plt.bar(ind - .2, rnd2, label='Random', width=set_width)
        plt.bar(ind - .2, select2, bottom=rnd2, label='select', width=set_width)
        plt.bar(ind - .2, exec2, bottom=rnd2 + select2, label='execute', width=set_width)

        # print(total_step - rnd2 - exec2 - select2)

        # for Matplotlib version < 1.5
        # plt.gca().set_color_cycle(None)
        # for Matplotlib version >= 1.5
        plt.gca().set_prop_cycle(None)

        replay_overhead = select_execute_replay - total_step
        # print(replay_overhead)

        rnd1 = rand_only
        exec1 = execute_replay - replay_overhead  # - select_execute_replay
        select1 = total_step - rnd1 - exec1

        plt.bar(ind + .2, rnd1, width=set_width)
        plt.bar(ind + .2, select1, bottom=rnd1, width=set_width)
        plt.bar(ind + .2, exec1, bottom=rnd1 + select1, width=set_width)

        # print(total_step - rnd1 - exec1 - select1)

        # print(df2['steps Mean point_estimate'][stepmask].append(df2['simulator'][stepmask]))

        # plt.bar(df2['steps Mean confidence_interval confidence_level'])

        # plt.subplot(1,num_plots,i+1)
        # print(rand_vals[i*N:i*N+N])
        # print(ind)
        # plt.bar(ind,rand_vals[i*N: i*N+N],label='Random')
        # plt.bar(ind,sel_vals[i*N: i*N+N],bottom=rand_vals[i*N: i*N+N],label='Select')
        # plt.bar(ind,exec_vals[i*N: i*N+N],bottom=sel_vals[i*N: i*N+N]+rand_vals[i*N: i*N+N],label='Exec')
        # plt.ylabel('time per step [ns]')
        # plt.title(titles[i])
        # plt.xticks(ind,["trad. parsing","code generation","opt. code gen."],rotation=90)

        plt.legend()

    plt.gcf().set_size_inches(12, 4, forward=True)
    plt.tight_layout()

    plt.savefig('profiling.pdf')
    plt.clf()


    convergences(filename)



if __name__ == "__main__":
    # use data as generated by script
    name = 'profilingData.csv'

    # use provided sample data
    name = 'sampleProfilingData.csv'

    plotProfilingData(name)