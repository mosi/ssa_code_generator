import sys
import os.path
import python_src.benchmarks
import toml

# Check if one argument was given
assert (len(sys.argv) == 2)

filename = sys.argv[1]

# Check if argument was file
assert (os.path.isfile(filename))


dat = python_src.benchmarks.model2toml(sys.argv[1])

newname = filename.split("/")[-1].split(".")[0]+".toml"

with  open(newname,'w') as f:
    toml.dump(dat, f)

print(filename + "\t-> "+newname)