import os

import textx
import jinja2
import random
import toml
import collections
import hashlib
from tqdm import tqdm

from base64 import urlsafe_b64encode



species_grammer ="""
Model: species*=Species ;
Species:
    name=ID '(' (initial=IntExpr)? ')' ';'
;

IntElem:
    (  ("round(" expr=FloatExpr ")")   | num=INT | constant=[IntConst]  )
;

IntFactor:
    (prefix=AddOp)? (elem=IntElem | "(" expr=IntExpr ")")
;

FactorOp:
    ("*"|"/")
;
IntTerm:
    left=IntFactor ( ops=FactorOp right=IntFactor)*
;

AddOp:
    ("+"|"-")
;

IntExpr:
    left=IntTerm ( ops=AddOp right=IntTerm)*
;

IntConst:
    name=ID ':' 'int' ('=' defaultValue=INT)? ';'
;

FloatElem:
    (num=FLOAT | constant=[FloatConst] | '#' spec=[Species] )
;

FloatFactor:
    (prefix=AddOp)? (elem=FloatElem | "(" expr=FloatExpr ")")
;

FactorOp:
    ("*"|"/")
;
FloatTerm:
    left=FloatFactor ( ops=FactorOp right=FloatFactor)*
;

FloatExpr:
    left=FloatTerm ( ops=AddOp right=FloatTerm)*
;

FloatConst:
    name=ID ':' 'float' ('=' defaultValue=FLOAT)? ';'
;
"""

constants_grammar ="""
Model: constants*=Constant ;
Constant:
    (intc=IntConst | floatc=FloatConst)
;
IntConst:
    name=ID ':' 'int' ('=' defaultValue=INT)? ';'
;
FloatConst:
    name=ID ':' 'float' ('=' defaultValue=FLOAT)? ';'
;

"""

class math_expression:
    def rekursive_tree(self,ex,is_product_at_highest_level=False):
            res = ""
            if ex is None:
                return res
            if ("Factor" in ex.__class__.__name__):
                if (ex.prefix):
                    res += ex.prefix
                if (ex.elem):
                    if (ex.elem.constant.__class__.__name__ == "IntConst"):
                        res += "__int_constant__:" + str(ex.elem.constant.name)
                        self.int_constants.add(ex.elem.constant.name)
                    elif (ex.elem.constant.__class__.__name__ == "FloatConst"):
                        res += "__float_constant__:" + str(ex.elem.constant.name)
                        self.float_constants.add(ex.elem.constant.name)
                    elif (hasattr(ex.elem,"expr") and ex.elem.expr.__class__.__name__ == "FloatExpr"):
                        assert(hasattr(ex.elem.expr,'left'))
                        expr = math_expression(ex.elem.expr)

                        self.species = self.species.union(expr.species)
                        self.int_constants = self.int_constants.union(expr.int_constants)
                        self.float_constants = self.float_constants.union(expr.float_constants)
                        res += "__my_rounding_function(" + expr.expr_str + ")"
                    elif (hasattr(ex.elem,"spec" )and ex.elem.spec.__class__.__name__ == "Species"):
                        res += "__species__:" + str(ex.elem.spec.name)
                        self.species.add(ex.elem.spec.name)
                        if(is_product_at_highest_level):
                            self.product_species.add(ex.elem.spec.name)
                    else:
                        res += str(ex.elem.num)

                elif (ex.expr):
                    res += "(" + self.rekursive_tree(ex.expr,is_product_at_highest_level) + ") "
                return res

            remains_product = ((not("+" in ex.ops or "-" in ex.ops)) if  (hasattr(ex, "ops")) else True )and is_product_at_highest_level

            res += self.rekursive_tree(ex.left,remains_product)


            for (o, e) in zip(ex.ops, ex.right):
                res += o + self.rekursive_tree(e,remains_product)
            return res

    def __init__(self,expression_tree):

        self.int_constants = set()
        self.float_constants = set()
        self.species = set()
        self.product_species = set()
        self.expr_str = self.rekursive_tree(expression_tree,is_product_at_highest_level=True)
        #print("Expresion: ",self.expr_str," at prod: ",self.product_species)
        assert(all(isinstance(x, str) for x in self.species))
        assert(isinstance(self.expr_str,str))



def line_of(substring, text):
    assert(substring in text)
    return text.split(substring)[0].count('\n') + 1

class Species(object):
    def __init__(self,parent,name,initial):
        self.parent = parent
        self.name = name
        self.initial = initial

def parse_custom_reaction_lang(filename):
    metamod = textx.metamodel_from_file('python_src/grammar.tx')
    with open(filename, 'r') as file:
        model_str_orig = file.read()
        print("\n # - starting file porse #")
    model_str = ""
    for line in model_str_orig.splitlines():
        model_str += line.split("//")[0]+"\n";

    #assert( "\nVARIABLES\n" in model_str)

    assert ("\nCONSTANTS\n" in model_str)
    #print("CONSTANTS in line ",line_of("\nCONSTANTS\n",model_str))

    assert ("\nSPECIES\n" in model_str)
    #print("SPECIES in line",line_of("\nSPECIES\n",model_str))
    assert(line_of("\nCONSTANTS\n",model_str) < line_of("\nSPECIES\n",model_str))

    assert ("\nRULES\n" in model_str)
    #print("RULES in line",line_of("RULES",model_str),"\n")
    assert(line_of("\nSPECIES\n",model_str) < line_of("\nRULES\n",model_str))

    constants_str = model_str.split("\nCONSTANTS\n")[1].split("\nSPECIES\n")[0]
    species_str = model_str.split("\nSPECIES\n")[1].split("\nRULES\n")[0]
    rules_str =  model_str.split("\nRULES\n")[1]

    constants_meta_model = textx.metamodel_from_str(constants_grammar)
    try:
        constants_model = constants_meta_model.model_from_str(constants_str)
    except textx.TextXError as e:
        raise Exception(
            'ERROR in CONSTANT ( --- with message: \n' + e.message)

    int_constants = []
    float_constants = []

    builtin_objects = {}

    print("Reading/Parsing Constants")
    if(hasattr(constants_model,"constants")):
        for c in tqdm(constants_model.constants):
            if(c.intc):
                int_constants.append(c.intc)
                builtin_objects[c.intc.name] = c.intc
            elif(c.floatc):
                float_constants.append(c.floatc)
                builtin_objects[c.floatc.name] = c.floatc





    species_meta_model = textx.metamodel_from_str(species_grammer,classes=[Species],builtins=builtin_objects)
    try:
        species_model = species_meta_model.model_from_str(species_str)
    except textx.TextXError as e:
        raise Exception(
            'ERROR in Species Definition ( --- with message: \n' + e.message)
    print("\nReading/Parsing Species")
    for s in tqdm(species_model.species):
        #print(get_math_expr(s.initial))
        line, col = species_model._tx_parser.pos_to_linecol(
            s._tx_position)
        tl = line_of(species_str.splitlines()[line - 1],model_str)
        s.description = "SPECIES(line " + str(tl) + "): " + species_str.splitlines()[line - 1]
        s.line = tl
        builtin_objects[s.name] = s
        assert(isinstance(s,Species))

    print("found ",len(species_model.species), " species in description")




    rule_meta_model = textx.metamodel_from_file('python_src/grammar_rule.tx',classes=[Species],builtins=builtin_objects)
    rules_list = []
    assert (not "@" in rules_str.split(';')[-1]) #check if last rule contains ;
    total_num_rules = len(rules_str.split(';')[:-1])
    print("\nReading/Parsing Rules")
    #all_rules = set()
    for (count,r) in enumerate(tqdm(rules_str.split(';')[:-1])):
        #if(count%100 == 0 and count > 0):
        #    print("Parsing rule ",count," of ", total_num_rules)
        #print("RULE: " + r)
        try:
            my_rule = rule_meta_model.model_from_str(r+';').rules
        except textx.TextXError as e:
            raise Exception('ERROR in Rule (line: '+ str(line_of(r,model_str)) + ':\n'+ r +'\n --- with message: \n'+ e.message)

        my_rule.original_description = r

        my_rule.line = line_of(r,model_str)

        my_rule.description = "RULE( "+str(count)+" line " + str(my_rule.line) + "): "+ r.replace("\n"," ");
        uid = hashlib.sha3_256((r + my_rule.description).encode('utf-8')).hexdigest()

        #print(uid)
        my_rule.uid = urlsafe_b64encode(bytes.fromhex(uid)).rstrip(b'=').decode('ascii').replace("-","_")
        rules_list.append(my_rule)

    #print("found ",len(rules_list), " rules in description")
    print("# - PARSING COMPLETE - #")

    model = {}
    model["observations"] = []
    model["rules"] = rules_list
    model["species"] = species_model.species

    model["float_constants"] = float_constants
    model["int_constants"] = int_constants
    if len(model["observations"]) == 0:
        model["observations"] = model["species"]

    num = 0
    for ent in model["observations"]:
        ent.observation_num = num
        num = num + 1


    # shuffle all elements of model
    if True:
        random.shuffle(model["species"])
        #model.species=sorted(model.species, key=lambda e: e.name)
        random.shuffle(model["rules"])
        #for r in model["rules"]:
        #    random.shuffle(r.inp)
        #    random.shuffle(r.out)


    return generate_dic(model,model_str)



def generate_dic(model,model_str):
    res = dict()


    res["species"] = {}

    res["float_constants"] = {}
    for k in model["float_constants"]:

        res["float_constants"][k.name] = {}
        if hasattr(k,"defaultValue"):
            res["float_constants"][k.name]["default"] = k.defaultValue

    res["int_constants"] = {}
    for k in model["int_constants"]:
        res["int_constants"][k.name] = {}
        if hasattr(k,"defaultValue"):
            res["int_constants"][k.name]["default"] = k.defaultValue

    for s in model["species"]:
        spec_dict = {}
        spec_dict["name"] = s.name
        spec_dict["line"] = s.line
        spec_dict["description"] = s.description
        initial_expr = math_expression(s.initial)
        spec_dict["initial"] = initial_expr.expr_str if initial_expr.expr_str != "" else 0
        assert(len(initial_expr.species) == 0)
        #assert(len(initial_expr.float_constants)== 0)
        spec_dict["initial_involved_int_constants"] = list(initial_expr.int_constants)
        spec_dict["initial_involved_float_constants"] = list(initial_expr.float_constants)
        res["species"][s.name] = spec_dict


    res["observations"] = {}

    for i in model["observations"]:
        res["observations"]["num_"+i.name] =  [i.name,]

    res["rules"] = []

    for (count,r) in enumerate(model["rules"]):
        rule_dict = dict()

        #the full inputs/outputs as specified in the model
        rule_dict["full_left"] = list(map(lambda e: e.name,r.inp))
        rule_dict["full_right"] = list(map(lambda e: e.name,r.out))

        #the actuall true inputs and outputs as they occur
        new_in = rule_dict['full_left'].copy()
        new_out = []
        for o in rule_dict['full_right']:
            if o in new_in:
                new_in.remove(o)
            else:
                new_out.append(o)

        rule_dict['consumed'] = new_in
        rule_dict['produced'] = new_out

        rule_dict["line"] = r.line
        rule_dict["description"] = r.description
        rule_dict["guards"] = []
        rule_dict["uid"] = r.uid


        species_in_guard = set()
        float_constants_in_guard = set()
        for g in r.guards:
            rule_dict["guards"].append( {
                "left" : math_expression(g.left).expr_str,
                "compOp" : g.comparator,
                "right" : math_expression(g.right).expr_str,
            })
            species_in_guard = species_in_guard.union(math_expression(g.left).species).union(math_expression(g.right).species)
            float_constants_in_guard = float_constants_in_guard.union(math_expression(g.left).float_constants).union(math_expression(g.right).float_constants)

        if r.rate.__class__.__name__ == "FloatProdExpr":
            rule_dict["rate_type"] = "simple_product"
        else:
            rule_dict["rate_type"] = "complex_rate"


        rate = math_expression(r.rate)
        rule_dict["rate_expression"] = rate.expr_str

        assert(rule_dict["rate_expression"] != "")
        rule_dict["species_in_rate"] = list(set(rate.species).union(species_in_guard).union(set(rule_dict["full_left"] )))
        rule_dict["float_constants_in_rate"] = list(set(rate.float_constants).union(float_constants_in_guard))


        #rule_dict["rate_nums"] =list(map(lambda e: e.num, filter(lambda mr: hasattr(mr, 'num') and mr.num >0, r.rate.elem)))
        #rule_dict["rate_species"] = list(map(lambda e: e.ent.name, filter(lambda mr: hasattr(mr, 'ent') and hasattr(mr.ent,'name'), r.rate.elem)))

        #works like this only for product rate

        for unaccounted in set(rule_dict["full_left"]).difference(set(rate.product_species)):
            rule_dict["guards"].append( {
                "left" : "__species__:" + str(unaccounted),
                "compOp" : ">=",
                "right" : str(rule_dict["full_left"].count(unaccounted)),
            })

        res["rules"].append(rule_dict)
    return res


#print(parse_custom_reaction_lang('models/predetor_prey.reaction_model'))
#print(parse_custom_reaction_lang('models/wnt.reaction_model'))
#print(parse_custom_reaction_lang('models/wnt_betaCat.reaction_model'))