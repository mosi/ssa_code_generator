def expand_wnt_model(numLR):
    with open('models/wnt_betaCat.reaction_model', 'r') as file:
        ms = file.read()

    new_lines = []


    for l in ms.splitlines():
        if '_inLR' in l:
            for i in range(numLR):
                new_lines.append(l.replace('_inLR', '_inLR_'+str(i)))
        else:
            new_lines.append(l)

    res_string = ""
    for l in new_lines:
        res_string+=l + "\n"

    f = open("models/gen_beta_cat_"+str(numLR)+".reaction_model", "w")
    f.write(res_string)
    f.close()


expand_wnt_model(100)
expand_wnt_model(1000)
expand_wnt_model(2000)