import itertools
import math
import os
from timeit import default_timer as timer
import jinja2
import subprocess
import json
from glob import glob
from prettytable import PrettyTable
import numpy as np
import random
import shutil
import toml
import pprint
import pandas as pd
import filecmp
from tqdm import tqdm
from multiprocessing import Pool

#from bngl_net_parser import *
#from react_lang_parser import *
#from ExpandWntModel import *

#for i in [2,4,8,16]:
#    expand_wnt_model(i)

#path_to_bionetgen_runn_network = "/home/tk381/Downloads/BioNetGen-2.5.0-linux/BioNetGen-2.5.0/bin/run_network"
#path_to_bionetgen_runn_network = "/home/tk381/tmp/bngl_code_gen/BioNetGen-2.5.0/bin/run_network"
from python_src.bngl_net_parser import parse_bngl_network, bngl_model_to_reaction_model
from python_src.react_lang_parser import parse_custom_reaction_lang


def model2toml(filename, replace_constants : bool):
    file_ext = filename.split('.')[-1]
    if file_ext == "net":
        with open(filename+".reaction_model",'w') as f:
            f.write( bngl_model_to_reaction_model( parse_bngl_network(filename,replace_constants)))
        return parse_custom_reaction_lang(filename+".reaction_model")
    elif file_ext == "reaction_model":
        return parse_custom_reaction_lang(filename)
    else:
        raise Exception("Unnokwn extension " + filename + " -> "+file_ext)


def preproccess_model_for_cpp(model):
    for s in model["species"]:
        for constant in model["species"][s]["initial_involved_int_constants"]:
            new_initial = model["species"][s]["initial"].replace("__int_constant__:"+constant,"int_constants["+str(model["int_constants"][constant]["num"])+"] /* " + constant + " */")
            model["species"][s]["initial"]=new_initial

        for constant in model["species"][s]["initial_involved_float_constants"]:
            new_initial = model["species"][s]["initial"].replace("__float_constant__:"+constant,"float_constants["+str(model["float_constants"][constant]["num"])+"] /* " + constant + " */")
            model["species"][s]["initial"]=new_initial


    for (count,rule) in enumerate(model["rules"]):
        for s in model["rules"][count]["species_in_rate"]:
            model["rules"][count]["rate_expression"] = model["rules"][count]["rate_expression"].replace("__species__:"+s,"static_cast<double>(populations["+str(model["species"][s]["num"])+"]) /* "+s+" */")
            for g in model["rules"][count]["guards"]:
                print(g)
                g["left"] = g["left"].replace("__species__:"+s,"static_cast<double>(populations["+str(model["species"][s]["num"])+"]) /* "+s+" */")
                g["right"] = g["right"].replace("__species__:" + s, "static_cast<double>(populations[" + str(
                    model["species"][s]["num"]) + "]) /* " + s + " */")
    for (count,rule) in enumerate(model["rules"]):
        for constant in model["rules"][count]["float_constants_in_rate"]:
            model["rules"][count]["rate_expression"] = model["rules"][count]["rate_expression"].replace("__float_constant__:" + constant, 
                                                                                                        "float_constants[{0}] /* {1} */".format(
                                                                                                            str(model[
                                                                                                                    "float_constants"][
                                                                                                                    constant][
                                                                                                                    "num"]),constant))
            for g in model["rules"][count]["guards"]:
                g["left"] = g["left"].replace(
                    "__float_constant__:" + constant,
                    "float_constants[{0}] /* {1} */".format(
                        str(model[
                                "float_constants"][
                                constant][
                                "num"]), constant))
                g["right"] = g["right"].replace(
                    "__float_constant__:" + constant,
                    "float_constants[{0}] /* {1} */".format(
                        str(model[
                                "float_constants"][
                                constant][
                                "num"]), constant))



    return model

def execute_this_command(x):
        subprocess.check_output(x,shell=True)

def grouper_it(n, iterable):
    it = iter(iterable)
    while True:
        chunk_it = itertools.islice(it, n)
        try:
            first_el = next(chunk_it)
        except StopIteration:
            return
        yield itertools.chain((first_el,), chunk_it)

def compile_cpp_code(model, ccompiler,lto :bool, single_file: bool):
    print("start compilation")

    flags = " -O3 -std=c++11 -march=native -c -o "

    if single_file:
        subprocess.check_output("sed -i '/#include/d' cpp/R*.cpp ", shell=True)
        subprocess.check_output("cat cpp/master_header.h cpp/*.cpp > cpp/all_merged.cpp", shell=True)

        subprocess.check_output(ccompiler +" cpp/all_merged.cpp  -O3 -std=c++11 -march=native -o prog_c.bin", shell=True)
        return

    full_compile_files = []
    for i in model["rules"]:
        full_compile_files.append("R_"+i["uid"]+ ".cpp")
    #full_compile_files.append("main.cpp")

    quick_compile_files = ["initialize.cpp","make_observations.cpp","read_constants.cpp","write_reaction_counts.cpp"]


    prefix = "cpp/"
    full_compile_files = map(lambda x: prefix+x,full_compile_files)
    quick_compile_files = map(lambda x: prefix + x, quick_compile_files)


    cmds = []


    chunksize = math.ceil(len(model["rules"])/3000)

    if (lto):
        flags = " -flto "+flags
    objects = " cpp/main.o "
    cmds.append(ccompiler + " cpp/main.cpp "+flags+" cpp/main.o ")
    for (n,i) in enumerate(grouper_it(chunksize,full_compile_files)):
        objname = " cpp/"+str(n)+".o"
        files = ""
        for f in i:
            files += " "+f
        if (len(f) > 1):
            newfilename = "cpp/o_"+str(n)+".cpp"
            cmds.append("cat "+files+ " > "+newfilename+" && "+ccompiler + " "+ newfilename + flags + objname)
        else:
            cmds.append( ccompiler  + files+flags + objname)
        objects += objname

    for i in quick_compile_files:
        cmds.append(ccompiler + " -std=c++11 -c -o " + i.replace(".cpp", ".o") + " " + i)
        objects += " " + i.replace(".cpp", ".o")

    #Execute the commands



    with Pool() as pool:
        for _ in tqdm(pool.imap_unordered(execute_this_command, cmds), total=len(cmds)):
            pass

    # linking
    print("--linking (if this takes to long don't use the -lto flag)")
    linkcmd = ccompiler + (" -flto " if lto else "") + " -O3  -march=native -o prog_c.bin " + objects
    #print(linkcmd)
    subprocess.check_output(linkcmd,shell=True)

    print("Compilation and Linking Done!")



def generate_cpp_code(model, ccompiler, lto=False, compile=True,use_single_file=False):

    os.system("rm cpp/*.cpp")

    print(" - start preprocess -")
    model = preproccess_model_for_cpp(model)

    print(" - preproccess complete -")

    assert (isinstance(model["until"], float))
    assert (model["num_steps"] > 1)

    with open('templates/main.cpp') as file_:
        template_main = jinja2.Template(file_.read())

    with open('templates/master_header.h') as file_:
        template_header = jinja2.Template(file_.read())

    with open('templates/reactions.cpp') as file_:
        template_reac = jinja2.Template(file_.read())

    with open('templates/initialize.cpp') as file_:
        template_init = jinja2.Template(file_.read())

    with open('cpp/initialize.cpp', 'w') as f:
        f.write(template_init.render(model=model))


    with open('templates/write_reaction_counts.cpp') as file_:
        template_write_counts = jinja2.Template(file_.read())
        with open('cpp/write_reaction_counts.cpp', 'w') as f:
            f.write(template_write_counts.render(model=model))

    with open('templates/make_observations.cpp') as file_:
        template_observations = jinja2.Template(file_.read())
        with open('cpp/make_observations.cpp', 'w') as f:
            f.write(template_observations.render(model=model))

    with open('cpp/main.cpp', 'w') as f:
        f.write(template_main.render(model=model))

    with open('templates/read_constants.cpp') as file_:
        template_constants = jinja2.Template(file_.read())

    with open('cpp/read_constants.cpp', 'w') as f:
        f.write(template_constants.render(model=model))

    reac_files = set(glob('cpp/R_*.cpp'))
    print("writing code for rules")
    for (count,r) in enumerate(tqdm(model["rules"])):
        file_name = 'cpp/R_'+r['uid']+'.cpp'
        if(file_name in reac_files):
            reac_files.remove(file_name)
        with open('cpp/dummy_'+r['uid']+'.cpp', 'w') as f:
            f.write(template_reac.render(model=model,rule=r))
        #os.system("clang-format -i cpp/dummy_"+r['uid']+".cpp")
        if not os.path.isfile(file_name) or not filecmp.cmp('cpp/dummy_'+r['uid']+'.cpp', file_name,shallow=False):
            #print("no save: ",file_name,filecmp.cmp('cpp/dummy.cpp', file_name))
            #os.system('diff '+file_name+ ' cpp/dummy.cpp')
            os.replace('cpp/dummy_'+r['uid']+'.cpp',file_name)
        else:
            os.remove('cpp/dummy_'+r['uid']+'.cpp')
            print("save reac")
    for f in reac_files:
        print(f," -> remove")
        os.remove(f)


    headier_name = 'cpp/master_header.h'
    with open('cpp/dummy.h', 'w') as f:
        f.write(template_header.render(model=model))
    if not os.path.isfile(headier_name) or not filecmp.cmp('cpp/dummy.h', headier_name,shallow=False):
        os.replace('cpp/dummy.h', headier_name)
        print("replace header")
    else:
        os.remove('cpp/dummy.h')
        print("save header")

    #os.system("clang-format -i cpp/*.cpp")

    compile_cpp_code(model,ccompiler,lto=lto,single_file=use_single_file)


def generate_rust_code(model, compile=True):
    assert (isinstance(model["until"], float))
    assert (model["num_steps"] > 1)

    thedir = "cpp"
    if not os.path.exists(thedir):
        os.makedirs(thedir)

    with open('templates/main_code_template.rs') as file_:
        template = jinja2.Template(file_.read())

    with open('templates/rule_module_template.rs') as file_:
        template_mod = jinja2.Template(file_.read())

    with open('cpp/bin.rs', 'w') as f:
        f.write(template.render(model=model))

    with open('cpp/reactions_mod_master.rs', 'w') as f:
        f.write(template_mod.render(model=model))

    with open('templates/lib_struct_template.rs') as file_:
        template_struct = jinja2.Template(file_.read())
    with open('cpp/lib.rs', 'w') as f:
        f.write(template_struct.render(model=model))
    print("Formatting Rust Code")
    subprocess.check_output("rustfmt cpp/*.rs", shell=True)

    # print(model['rules'][0])
    if compile:
        start_compile = timer()
        print("# Compiling Rust Code")
        subprocess.check_output("cargo build -j 1 --manifest-path=generated_code/Cargo.toml --release", shell=True,
                                stderr=subprocess.STDOUT)
        compile_time = timer() - start_compile
        return compile_time






def measure_gcc_variant(mdl):
    compile_time = []
    # steps = []
    time_per_step = []
    for _ in range(3):
        result = {}
        m = model2toml(mdl)
        m = shuffle_model(m)
        preprocess_model(m)
        m["num_steps"] = 1000
        m["until"] = 20.0
        compile_time.append(generate_cpp_code(m))
        myCmd = os.popen('./prog_gcc.bin').read()
        for i in myCmd.split("\n"):
            if "#" in i:
                i = i.split(" ")
                # print(i)
                assert (i[1] == "-C-")
                time_per_step.append(float(i[5]) / int(i[2]) * 1e6)

    result = {}
    result["compile ave"] = np.average(compile_time)
    result["compile std"] = np.std(compile_time)

    result["step ave"] = np.average(time_per_step)
    result["step std"] = np.std(time_per_step)
    return result




def fix_numbers(mod):
    mod2 = mod.copy()
    new_affe = {}
    for k in mod2["affected_reaction"].keys():
        new_affe[str(k)] = mod["affected_reaction"][k]
    mod2["affected_reaction"] = new_affe

    return mod2

def generate_parsed(mod):
    mod2 = mod.copy()
    new_affe = {}
    for k in mod2["affected_reaction"].keys():
        new_affe[str(k)] = mod["affected_reaction"][k]
    mod2["affected_reaction"] = new_affe

    toml.dump(mod2, open("model_as_parsed.toml", "w"))
    toml.dump(mod2, open("parsed_runner/model_as_parsed.toml", "w"))


def optimize_model_order(mod, opt_file):
    counts = toml.load(opt_file)
    ave = np.average(np.array(list(counts.values())))
    mod['rules'] = list(sorted(mod['rules'], key=lambda x: counts[x['uid']] if x['uid'] in counts else ave, reverse=True))
    #for i in mod['rules']:
    #    print (i['uid'], ' -> ',counts[i['uid']])
    for (count,r) in enumerate(mod["rules"]):
        r['num'] = count

    return mod


def run_network(model_file, simulator, steps, until, do_sort=False, print_output=False):
    timings = {}

    start_parse = timer()
    m = model2toml(model_file)
    # print(m)
    m = shuffle_model(m)
    unprocesd_model = m.copy()
    # print(unprocesd_model)
    preprocess_model(m)
    m["num_steps"] = int(steps)
    m["until"] = float(until)
    stop_parse = timer()

    subprocess.check_output('cargo build -j 1 --manifest-path=parsed_runner_counting/Cargo.toml --release', shell=True,
                            stderr=subprocess.STDOUT)
    start_sort = timer()
    stop_sort = start_sort
    if do_sort:
        if os.path.exists("reaction_counts.toml"):
            os.remove("reaction_counts.toml")
        generate_parsed(m)

        subprocess.check_output('cargo run --manifest-path=parsed_runner_counting/Cargo.toml --release', shell=True,
                                stderr=subprocess.STDOUT)
        m = optimize_model_order(unprocesd_model, "reaction_counts.toml")
        m["num_steps"] = int(steps)
        m["until"] = float(until)
        preprocess_model(m)
        stop_sort = timer()
        # print("tsort = "+ str(stop_sort-start_sort))

    start_compile = timer()
    reported_time = None
    reported_steps = None
    if simulator == "rust":
        generate_rust_code(m)
        stop_compile = timer()
        start_run = timer()
        # out = subprocess.check_output("cargo run --manifest-path=generated_code/Cargo.toml --release", shell=True,stderr=subprocess.STDOUT)
        out = subprocess.check_output("generated_code/target/release/run_model", shell=True,
                                      stderr=subprocess.STDOUT)
        stop_run = timer();
        prop_time = out.decode("utf-8").split("Steps in ")[1].split(" -> ")[0]
        reported_time = float(prop_time)
        reported_steps = int(out.decode("utf-8").split("# --- ")[1].split("Steps in")[0])
        if print_output:
            print(out.decode("utf-8"))

    elif simulator == "parsed":
        generate_parsed(m)
        stop_compile = timer()
        subprocess.check_output('cargo build -j 1 --manifest-path=parsed_runner/Cargo.toml --release', shell=True,
                                stderr=subprocess.STDOUT)
        start_run = timer()
        # out = subprocess.check_output('cargo run --manifest-path=parsed_runner/Cargo.toml --release', shell=True,stderr=subprocess.STDOUT)
        out = subprocess.check_output('parsed_runner/target/release/run_model', shell=True,
                                      stderr=subprocess.STDOUT)
        stop_run = timer()
        prop_time = out.decode("utf-8").split("Steps in ")[1].split(" s -> ")[0]
        reported_steps = int(out.decode("utf-8").split("# --- ")[1].split("Steps in")[0])
        reported_time = float(prop_time)
        if print_output:
            print(out)
    elif simulator == "gcc":
        if os.path.exists("prog_c.bin"):
            os.remove("prog_c.bin")
        generate_cpp_code(m, "g++")
        stop_compile = timer()
        start_run = timer()
        out = subprocess.check_output('./prog_c.bin', shell=True)
        stop_run = timer()
        prop_time = out.decode("utf-8").split("Steps in ")[1].split(" s -> ")[0]
        reported_time = float(prop_time)
        reported_steps = int(out.decode("utf-8").split("# -C- ")[1].split("Steps in")[0])
        if print_output:
            print(out)
    elif simulator == "clang":
        if os.path.exists("prog_c.bin"):
            os.remove("prog_c.bin")
        generate_cpp_code(m, "clang++")
        stop_compile = timer()
        start_run = timer()
        out = subprocess.check_output('./prog_c.bin', shell=True)
        stop_run = timer()
        prop_time = out.decode("utf-8").split("Steps in ")[1].split(" s -> ")[0]
        reported_time = float(prop_time)
        reported_steps = int(out.decode("utf-8").split("# -C- ")[1].split("Steps in")[0])
        if print_output:
            print(out)
    elif simulator == "bionetgen":
        stop_compile = timer()

        start_run = timer()
        bng_cmd = path_to_bionetgen_runn_network + " -o ./bngl_output -p ssa -h " + str(random.randint(10,
                                                                                                       1000000000)) + " --cdat 1 --fdat 0 -g ./" + model_file + " " + " ./" + model_file + " " + str(
            m["until"] / m["num_steps"]) + " " + str(m["num_steps"])

        # bng_cmd = "/home/tk381/Downloads/BioNetGen-2.5.0-linux/BioNetGen-2.5.0/bin/run_network -o ./bngl_output -p ssa -h 1652213817 --cdat 1 --fdat 0 -g ./models/testmodel.net ./models/testmodel.net 1.0 120"
        print(bng_cmd)
        out = subprocess.check_output(bng_cmd, shell=True)

        stop_run = timer()
        prop_time = out.decode("utf-8").split("Propagation took")[1].split("CPU seconds")[0]
        reported_time = float(prop_time)
        reported_steps = int(out.decode("utf-8").split("TOTAL STEPS: ")[1].split("Time course of")[0])
        if print_output:
            print(str(out.decode("utf-8")))
    else:
        raise Exception("Unsupported Simulator " + str(simulator))

    return {"t_parsing": stop_parse - start_parse, "t_sort": stop_sort - start_sort,
            "t_compile": stop_compile - start_compile, "t_total_run": stop_run - start_run, "t_reported": reported_time,
            "num_steps": reported_steps}


def measure_total_runtime(reps,extra_name=''):
    target_time = 100.0
    iosteps = 1000
    df = pd.DataFrame()
    for modelname in ['models/wnt_betaCat.reaction_model','models/gen_beta_cat_2.reaction_model','models/gen_beta_cat_4.reaction_model','models/gen_beta_cat_8.reaction_model','models/gen_beta_cat_16.reaction_model']:
        #['models/multisite2.net', 'models/multistate.net',
                     # 'models/egfr_net.net']:  ##'models/multisite2.net',
        for _ in range(reps):
            order = list(["parsed", "bionetgen", "rust", "gcc" ,"clang"])
            random.shuffle(order)
            for simulator in order:
                for do_sort in [False, True]:
                    if simulator == 'bionetgen' and modelname.split('.')[-1]=='reaction_model':
                        continue
                    if do_sort and simulator == "bionetgen":
                        continue
                    res = run_network(modelname, simulator, iosteps, target_time, do_sort=do_sort, print_output=True)
                    res["Simulator"] = simulator
                    res["do_sort"] = do_sort
                    res["until"] = target_time
                    res["iosteps"] = iosteps
                    res["model"] = modelname
                    df = df.append(res, ignore_index=True)
                df.to_csv("runtimes_partial"+extra_name+".csv")
    df.to_csv("runtimes"+extra_name+".csv")
    print(df)


def profiling_benchmarks(reps,extra_name = ''):
    df = pd.DataFrame()
    for model_file in ['models/wnt_betaCat.reaction_model','models/gen_beta_cat_2.reaction_model','models/gen_beta_cat_4.reaction_model','models/gen_beta_cat_8.reaction_model','models/gen_beta_cat_16.reaction_model']:#[ 'models/predPrey.net', 'models/multisite2.net', 'models/multistate.net', 'models/egfr_net.net']:  # ]:#,'models/egfr_net.net']:
        # current_model = model_file.split('/')[1]
        for _ in range(reps):
            run_types = ["sorted parsed", "compiled", "parsed", "sorted compiled"]
            random.shuffle(run_types)
            for s in run_types:
                for res in profiling_measurement(model_file, s):
                    df = df.append(res, ignore_index=True)
                df.to_csv("profiling_partial"+extra_name+".csv")
    df.to_csv("profiling"+extra_name+".csv")


# profiling_benchmarks()



from math import log10, floor
def round_sig(x, sig=6, small_value=1.0e-9):
    return round(x, sig - int(floor(log10(max(abs(x), abs(small_value))))) - 1)



"""
measure_total_runtime(5,'wntA')
measure_total_runtime(15,'wntB')
measure_total_runtime(30,'wntA')

profiling_benchmarks(4,'wntA')
profiling_benchmarks(10,'wntB')
profiling_benchmarks(30,'wntC')
exit()

#measure_total_runtime(10,'A')

#give_table_of_models()
try:
    measure_total_runtime(10,'A')
except:
    print("Error RUNTIME")

try:
    measure_total_runtime(20,'B')
except:
    print("Error RUNTIME")

try:
    measure_total_runtime(100,'C')
except:
    print("Error RUNTIME")


try:
    profiling_benchmarks(10,'A')
except:
    print("Error RUNTIME")

try:
    profiling_benchmarks(20,'B')
except:
    print("Error RUNTIME")

try:
    profiling_benchmarks(100,'C')
except:
    print("Error RUNTIME")

"""